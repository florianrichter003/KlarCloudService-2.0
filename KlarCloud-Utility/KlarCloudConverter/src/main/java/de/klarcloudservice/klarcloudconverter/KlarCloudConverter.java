/*
 * © Copyright - Emmanuel Lampe aka. rexlManu 2019.
 */
package de.klarcloudservice.klarcloudconverter;

import de.klarcloudservice.utility.files.FileUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Arrays;
import java.util.Properties;

/******************************************************************************************
 *    Urheberrechtshinweis                                                                *
 *    Copyright © Emmanuel Lampe 2019                                                  *
 *    Erstellt: 25.01.2019 / 20:47                                               *
 *                                                                                        *
 *    Alle Inhalte dieses Quelltextes sind urheberrechtlich geschützt.                    *
 *    Das Urheberrecht liegt, soweit nicht ausdrücklich anders gekennzeichnet,            *
 *    bei Emmanuel Lampe. Alle Rechte vorbehalten.                                        *
 *                                                                                        * 
 *    Jede Art der Vervielfältigung, Verbreitung, Vermietung, Verleihung,                 *
 *    öffentlichen Zugänglichmachung oder andere Nutzung                                  *
 *    bedarf der ausdrücklichen, schriftlichen Zustimmung von Emmanuel Lampe.             *
 ******************************************************************************************/

public final class KlarCloudConverter {

    private static final String ROOT_DIRECTORY = "klarcloud/";

    public void onStart() {
        if (! this.fileExist("configuration.properties")) {
            System.out.println("No installation for 'KlarCloudService' could be found");
            return;
        }
        if (this.fileExist("ControllerKEY")) this.addControllerKeyToConfiguration();
        if (this.fileExists("clients.json", "messages.json", "users.json", "signs", "addons"))
            this.moveFilesToRootDirectory("clients.json", "messages.json", "users.json", "signs", "addons");
        System.out.println("The migration was successful.");
        System.out.println("Now you can start the KlarCloudService! :)");
    }

    public void onStop() {
        FileUtils.deleteOnExit(Paths.get(FileUtils.getInternalFileName()));
    }

    private void moveFile(String file, String newPath) {
        try {
            Files.move(Paths.get(file), Paths.get(newPath, file), StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException ignored) {
        }
    }

    private void moveFilesToRootDirectory(String... paths) {
        Arrays.stream(paths).forEach(path -> this.moveFile(path, ROOT_DIRECTORY));
    }

    private void addControllerKeyToConfiguration() {
        File configurationProperties = getPathAsFile("configuration.properties");
        Properties properties = new Properties();
        try {
            properties.load(new FileInputStream(configurationProperties));
            File controllerKey = this.getPathAsFile("ControllerKEY");
            properties.setProperty("controller.key", FileUtils.readFileAsString(controllerKey));
            FileUtils.deleteFileIfExists(controllerKey.toPath());
            properties.save(new FileOutputStream(configurationProperties), "KlarCloudConverter");
        } catch (IOException ignored) {
        }
    }

    private File getPathAsFile(String path) {
        return new File(path);
    }

    private boolean fileExists(String... paths) {
        return Arrays.stream(paths).allMatch(this::fileExist);
    }

    private boolean fileExist(String path) {
        return this.getPathAsFile(path).exists();
    }
}
