/*
 * © Copyright - Emmanuel Lampe aka. rexlManu 2019.
 */
package de.klarcloudservice.klarcloudconverter.bootstrap;

import de.klarcloudservice.KlarCloudLibrary;
import de.klarcloudservice.klarcloudconverter.KlarCloudConverter;

/******************************************************************************************
 *    Urheberrechtshinweis                                                                *
 *    Copyright © Emmanuel Lampe 2019                                                  *
 *    Erstellt: 25.01.2019 / 20:48                                               *
 *                                                                                        *
 *    Alle Inhalte dieses Quelltextes sind urheberrechtlich geschützt.                    *
 *    Das Urheberrecht liegt, soweit nicht ausdrücklich anders gekennzeichnet,            *
 *    bei Emmanuel Lampe. Alle Rechte vorbehalten.                                        *
 *                                                                                        * 
 *    Jede Art der Vervielfältigung, Verbreitung, Vermietung, Verleihung,                 *
 *    öffentlichen Zugänglichmachung oder andere Nutzung                                  *
 *    bedarf der ausdrücklichen, schriftlichen Zustimmung von Emmanuel Lampe.             *
 ******************************************************************************************/

public final class KlarCloudConverterLaunch {

    public static synchronized void main(String[] args) {
        KlarCloudLibrary.sendHeader();
        KlarCloudConverter klarCloudConverter = new KlarCloudConverter();
        Runtime.getRuntime().addShutdownHook(new Thread(klarCloudConverter::onStop));
        klarCloudConverter.onStart();
    }
}
