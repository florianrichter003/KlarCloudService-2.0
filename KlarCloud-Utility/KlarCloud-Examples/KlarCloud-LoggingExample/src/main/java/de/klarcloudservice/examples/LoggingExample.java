/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.examples;

import de.klarcloudservice.logging.KlarCloudConsoleLogger;

import java.io.IOException;

/**
 * @author _Klaro | Pasqual K. / created on 27.12.2018
 */

public class LoggingExample {
    public LoggingExample() throws IOException {
        final KlarCloudConsoleLogger klarCloudConsoleLogger = new KlarCloudConsoleLogger(false /*with or without message colour*/ );

        klarCloudConsoleLogger.info("Hello boy"); //Sends a INFO message into the console
    }
}
