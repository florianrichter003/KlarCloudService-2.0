# KlarCloudService 2.0

An efficient Minecraft-Network Management System

[![Codacy Badge](https://api.codacy.com/project/badge/Grade/9500818dfbb246ce9f0872bbde6fa28a)](https://www.codacy.com?utm_source=gitlab.com&amp;utm_medium=referral&amp;utm_content=_Klaro/KlarCloudService-2.0&amp;utm_campaign=Badge_Grade)
[![pipeline status](https://gitlab.com/_Klaro/KlarCloudService-2.0/badges/master/pipeline.svg)](https://gitlab.com/_Klaro/KlarCloudService-2.0/commits/master)

![LICENCE](https://img.shields.io/badge/license-Apache--2.0-brightgreen.svg)

### Prerequisites

To run KlarCloud you need 
 * at least 2GB DDR 3 Memory
 * at least 2vCores
 * at least Java 8 (recommended Java 8)
 * GNU Screen (sudo apt-get install screen)

## Built With

* [Maven](https://maven.apache.org/) - Dependency Management

## Contributing

Please read [CONTRIBUTING.md](https://gitlab.com/_Klaro/KlarCloudService-2.0/blob/master/CONTRIBUTING.md) for details on our code of conduct, and the process for submitting pull requests to us.

## Authors

* **Pasqual K.** - *Initial work* - [Profile](https://gitlab.com/_Klaro)

See also the list of [contributors](https://gitlab.com/_Klaro/KlarCloudService-2.0/graphs/master) who participated in this project.

## License

This project is licensed under the Apache-2.0 License - see the [LICENSE.md](LICENSE) file for details

## Support

If you have questions, found a bug or have a suggestion, feel free to join our [Discord Server](https://discord.gg/fwe2CHD)

## Developers

Feel free to commit into the 'global-commits' branch if you found a bug.
