/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.signaddon;

import com.google.common.annotations.Beta;
import com.google.common.base.Enums;
import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import de.klarcloudservice.KlarCloudAPISpigot;
import de.klarcloudservice.KlarCloudLibrary;
import de.klarcloudservice.bootstrap.SpigotBootstrap;
import de.klarcloudservice.internal.events.CloudServerAddEvent;
import de.klarcloudservice.internal.events.CloudServerInfoUpdateEvent;
import de.klarcloudservice.internal.events.CloudServerRemoveEvent;
import de.klarcloudservice.meta.info.ServerInfo;
import de.klarcloudservice.meta.server.ServerGroup;
import de.klarcloudservice.netty.packets.PacketOutRequestSignUpdate;
import de.klarcloudservice.netty.packets.PacketOutRequestsSigns;
import de.klarcloudservice.signs.KlarCloudSign;
import de.klarcloudservice.signs.SignLayout;
import de.klarcloudservice.signs.SignLayoutConfiguration;
import de.klarcloudservice.signs.SignPosition;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

import javax.management.InstanceAlreadyExistsException;
import java.util.Map;
import java.util.UUID;

/**
 * @author _Klaro | Pasqual K. / created on 11.12.2018
 */

@Getter
@Setter
@Beta
public final class SignSelector {
    @Getter
    private static SignSelector instance;

    private SignLayoutConfiguration signLayoutConfiguration;
    private Worker worker;
    private Map<UUID, KlarCloudSign> signMap;

    /**
     * Creates a new SignSelector instance
     *
     * @throws Throwable
     */
    public SignSelector() throws Throwable {
        if (instance == null) instance = this;
        else throw new InstanceAlreadyExistsException();

        KlarCloudAPISpigot.getInstance().getChannelHandler().sendPacketAsynchronous("KlarCloudController", new PacketOutRequestsSigns());
        SpigotBootstrap.getInstance().getServer().getMessenger().registerOutgoingPluginChannel(SpigotBootstrap.getInstance(), "BungeeCord");
        SpigotBootstrap.getInstance().getServer().getPluginManager().registerEvents(new ListenerImpl(), SpigotBootstrap.getInstance());

        SpigotBootstrap.getInstance().getServer().getScheduler().runTaskLaterAsynchronously(SpigotBootstrap.getInstance(), () -> {
            this.worker = new Worker(this.signLayoutConfiguration.getLoadingLayout().getPerSecondAnimation());
            this.worker.setDaemon(true);
            this.worker.start();

            for (ServerInfo serverInfo : KlarCloudAPISpigot.getInstance().getInternalCloudNetwork().getServerProcessManager().getAllRegisteredServerProcesses()) {
                if (! serverInfo.getCloudProcess().getName().equals(KlarCloudAPISpigot.getInstance().getServerInfo().getCloudProcess().getName())) {
                    final KlarCloudSign klarCloudSign = findFreeSign(serverInfo.getServerGroup().getName());
                    if (klarCloudSign != null)
                        updateSign(klarCloudSign, serverInfo);
                }
            }
        }, 120L);
    }

    public void updateAll() {
        KlarCloudAPISpigot.getInstance().getChannelHandler().sendPacketAsynchronous("KlarCloudController", new PacketOutRequestSignUpdate());
    }

    private void setMaintenance(final KlarCloudSign klarCloudSign) {
        set(toNormalSign(klarCloudSign.getSignPosition()), this.getGroupLayout(klarCloudSign).getMaintenanceLayout(), null, KlarCloudAPISpigot.getInstance().getInternalCloudNetwork().getServerGroups().get(klarCloudSign.getSignPosition().getTargetGroup()));
    }

    private void setEmpty(final KlarCloudSign klarCloudSign) {
        set(toNormalSign(klarCloudSign.getSignPosition()), this.getGroupLayout(klarCloudSign).getEmptyLayout(), klarCloudSign.getServerInfo(), null);
    }

    private void setFull(final KlarCloudSign klarCloudSign) {
        set(toNormalSign(klarCloudSign.getSignPosition()), this.getGroupLayout(klarCloudSign).getFullLayout(), klarCloudSign.getServerInfo(), null);
    }

    private void setOnline(final KlarCloudSign klarCloudSign) {
        set(toNormalSign(klarCloudSign.getSignPosition()), this.getGroupLayout(klarCloudSign).getOnlineLayout(), klarCloudSign.getServerInfo(), null);
    }

    private void setLoading(final KlarCloudSign klarCloudSign, SignLayout animation) {
        set(toNormalSign(klarCloudSign.getSignPosition()), animation, null, KlarCloudAPISpigot.getInstance().getInternalCloudNetwork().getServerGroups().get(klarCloudSign.getSignPosition().getTargetGroup()));
    }

    private SignLayout.GroupLayout getGroupLayout(final KlarCloudSign klarCloudSign) {
        SignLayout.GroupLayout groupLayout = this.signLayoutConfiguration.getGroupLayouts().get(klarCloudSign.getSignPosition().getTargetGroup());
        return groupLayout != null ? groupLayout : this.signLayoutConfiguration.getDefaultLayout();
    }

    private void set(final Sign sign, final SignLayout layout, final ServerInfo serverInfo, final ServerGroup serverGroup) {
        if (sign == null) return;
        org.bukkit.material.Sign signData = (org.bukkit.material.Sign) sign.getData();
        if (signData == null) return;

        if (layout == null && serverInfo == null && serverGroup == null) {
            String[] lines = new String[]{" ", " ", " ", " "};
            SpigotBootstrap.getInstance().getServer().getOnlinePlayers().forEach(e -> e.sendSignChange(sign.getLocation(), lines));
            return;
        }

        if (signData.isWallSign()) {
            Block block = sign.getLocation().getBlock().getRelative(signData.getAttachedFace());
            Material material = Enums.getIfPresent(Material.class, layout.getMaterialName().toUpperCase()).orNull();
            if (material != null) {
                block.setType(material, true);
                block.setData((byte) layout.getMaterialData());
            }
        }

        final String[] lines = layout.getLines().clone();

        if (serverInfo == null && serverGroup != null) {
            for (int i = 0; i < 3; i++)
                lines[i] = ChatColor.translateAlternateColorCodes('&', lines[i]
                        .replace("%group%", serverGroup.getName()).replace("%client%", serverGroup.getClient()));
        } else if (serverInfo != null) {
            for (int i = 0; i < 4; i++) {
                lines[i] = ChatColor.translateAlternateColorCodes('&', lines[i]
                        .replace("%group%", serverInfo.getServerGroup().getName())
                        .replace("%server%", serverInfo.getCloudProcess().getName())
                        .replace("%motd%", serverInfo.getMotd())
                        .replace("%online_players%", Integer.toString(serverInfo.getOnlinePlayers().size()))
                        .replace("%max_players%", Integer.toString(serverInfo.getServerGroup().getAdvancedConfiguration().getMaxPlayers()))
                        .replace("%client%", serverInfo.getCloudProcess().getClient()));
            }
        }

        SpigotBootstrap.getInstance().getServer().getOnlinePlayers().forEach(e -> e.sendSignChange(sign.getLocation(), lines));
    }

    private boolean isOnSign(final ServerInfo serverInfo) {
        for (KlarCloudSign klarCloudSign : this.signMap.values())
            if (klarCloudSign.getServerInfo() != null && klarCloudSign.getServerInfo().getCloudProcess().getName().equals(serverInfo.getCloudProcess().getName()))
                return true;
        return false;
    }

    public void handleCreateSign(final KlarCloudSign klarCloudSign) {
        this.signMap.put(klarCloudSign.getUuid(), klarCloudSign);
    }

    public void handleSignRemove(final KlarCloudSign klarCloudSign) {
        this.signMap.remove(klarCloudSign.getUuid());
        updateSign(klarCloudSign);
    }

    private void updateSign(final KlarCloudSign klarCloudSign, final ServerInfo serverInfo) {
        klarCloudSign.setServerInfo(serverInfo);
    }

    private void updateSign(final KlarCloudSign klarCloudSign) {
        klarCloudSign.setServerInfo(null);
        set(toNormalSign(klarCloudSign.getSignPosition()), null, null, null);
    }

    private Sign toNormalSign(final SignPosition position) {
        final Block block = toLocation(position).getBlock();
        if (! (block.getState() instanceof Sign))
            return null;
        return (Sign) block.getState();
    }

    private Location toLocation(final SignPosition position) {
        return new Location(Bukkit.getWorld(position.getWorld()), position.getX(), position.getY(), position.getZ());
    }

    public SignPosition toSignPosition(final String group, final Location location) {
        return new SignPosition(group, location.getWorld().getName(), location.getBlockX(), location.getBlockY(), location.getBlockZ());
    }

    private KlarCloudSign findFreeSign(final String group) {
        for (KlarCloudSign klarCloudSign : this.signMap.values())
            if (klarCloudSign.getServerInfo() == null && klarCloudSign.getSignPosition().getTargetGroup().equals(group))
                return klarCloudSign;

        return null;
    }

    private KlarCloudSign findSign(final ServerInfo serverInfo) {
        for (KlarCloudSign klarCloudSign : this.signMap.values())
            if (klarCloudSign.getServerInfo() != null && klarCloudSign.getServerInfo().getCloudProcess().getName().equals(serverInfo.getCloudProcess().getName()))
                return klarCloudSign;

        return null;
    }

    private KlarCloudSign getSign(final SignPosition signPosition) {
        for (KlarCloudSign klarCloudSign : this.signMap.values()) {
            if (klarCloudSign.getSignPosition().getWorld().equals(signPosition.getWorld())
                    && klarCloudSign.getSignPosition().getX() == signPosition.getX()
                    && klarCloudSign.getSignPosition().getY() == signPosition.getY()
                    && klarCloudSign.getSignPosition().getZ() == signPosition.getZ()) {
                return klarCloudSign;
            }
        }

        return null;
    }

    public KlarCloudSign getSign(final Location location) {
        return getSign(toSignPosition(null, location));
    }

    private class Worker extends Thread {
        Worker(final int animations) {
            this.animations = animations;
        }

        private int animations;
        private SignLayout currentLoadingLayout;

        @Override
        public void run() {
            while (true) {
                this.currentLoadingLayout = SignSelector.this.signLayoutConfiguration.getLoadingLayout().getNextLayout();

                KlarCloudAPISpigot.getInstance().getInternalCloudNetwork().getServerProcessManager().getAllRegisteredServerProcesses().forEach(e -> {
                    if (! e.getCloudProcess().getName().equals(KlarCloudAPISpigot.getInstance().getServerInfo().getCloudProcess().getName()) && ! SignSelector.this.isOnSign(e)) {
                        final KlarCloudSign klarCloudSign = findFreeSign(e.getServerGroup().getName());
                        if (klarCloudSign != null)
                            SignSelector.this.updateSign(klarCloudSign, e);
                    }
                });

                for (KlarCloudSign klarCloudSign : SignSelector.this.signMap.values()) {
                    if (klarCloudSign == null)
                        continue;

                    final ServerGroup serverGroup = KlarCloudAPISpigot.getInstance().getInternalCloudNetwork().getServerGroups().get(klarCloudSign.getSignPosition().getTargetGroup());
                    if (serverGroup == null)
                        continue;

                    if (SpigotBootstrap.getInstance().isEnabled()) {
                        SpigotBootstrap.getInstance().getServer().getScheduler().runTask(SpigotBootstrap.getInstance(), () -> {
                            if (klarCloudSign.getServerInfo() == null)
                                SignSelector.this.setLoading(klarCloudSign, this.currentLoadingLayout);
                            else if (serverGroup.isMaintenance())
                                SignSelector.this.setMaintenance(klarCloudSign);
                            else if (klarCloudSign.getServerInfo().getOnlinePlayers().size() == 0)
                                SignSelector.this.setEmpty(klarCloudSign);
                            else if (klarCloudSign.getServerInfo().getOnlinePlayers().size() == serverGroup.getAdvancedConfiguration().getMaxPlayers())
                                SignSelector.this.setFull(klarCloudSign);
                            else
                                SignSelector.this.setOnline(klarCloudSign);
                        });
                    } else
                        return;
                }

                KlarCloudLibrary.sleep(Worker.this, 1000 / this.animations);
            }
        }
    }

    private final class ListenerImpl implements Listener {
        @EventHandler(priority = EventPriority.LOW)
        public void handle(final PlayerInteractEvent event) {
            if ((event.getAction().equals(Action.RIGHT_CLICK_BLOCK))
                    && (event.getClickedBlock().getType().equals(Material.SIGN_POST)
                    || event.getClickedBlock().getType().equals(Material.WALL_SIGN))) {
                final KlarCloudSign klarCloudSign = getSign(event.getClickedBlock().getLocation());
                if (klarCloudSign != null && klarCloudSign.getServerInfo() != null && ! klarCloudSign.getServerInfo().getServerGroup().isMaintenance()) {
                    ByteArrayDataOutput byteArrayDataOutput = ByteStreams.newDataOutput();
                    byteArrayDataOutput.writeUTF("Connect");
                    byteArrayDataOutput.writeUTF(klarCloudSign.getServerInfo().getCloudProcess().getName());
                    event.getPlayer().sendPluginMessage(SpigotBootstrap.getInstance(), "BungeeCord", byteArrayDataOutput.toByteArray());
                }
            }
        }

        @EventHandler
        public void handle(final CloudServerRemoveEvent event) {
            final KlarCloudSign klarCloudSign = findSign(event.getServerInfo());
            if (klarCloudSign != null)
                updateSign(klarCloudSign);
        }

        @EventHandler
        public void handle(final CloudServerInfoUpdateEvent event) {
            if (! event.getServerInfo().getCloudProcess().getName().equals(KlarCloudAPISpigot.getInstance().getServerInfo().getCloudProcess().getName())) {
                final KlarCloudSign klarCloudSign = findSign(event.getServerInfo());
                if (klarCloudSign != null)
                    updateSign(klarCloudSign, event.getServerInfo());
            }
        }

        @EventHandler
        public void handle(final CloudServerAddEvent event) {
            KlarCloudSign klarCloudSign = findSign(event.getServerInfo());
            if (klarCloudSign != null)
                updateSign(klarCloudSign, event.getServerInfo());
            else {
                klarCloudSign = findFreeSign(event.getServerInfo().getServerGroup().getName());
                if (klarCloudSign != null)
                    updateSign(klarCloudSign, event.getServerInfo());
            }
        }
    }
}
