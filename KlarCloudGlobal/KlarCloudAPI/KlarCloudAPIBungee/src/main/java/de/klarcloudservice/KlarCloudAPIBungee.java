/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice;

import de.klarcloudservice.bootstrap.BungeecordBootstrap;
import de.klarcloudservice.configurations.Configuration;
import de.klarcloudservice.event.EventManager;
import de.klarcloudservice.listener.CloudConnectListener;
import de.klarcloudservice.logging.KlarCloudConsoleLogger;
import de.klarcloudservice.meta.info.ProxyInfo;
import de.klarcloudservice.meta.proxy.ProxyGroup;
import de.klarcloudservice.meta.server.ServerGroup;
import de.klarcloudservice.meta.startup.ProxyStartupInfo;
import de.klarcloudservice.netty.NettyHandler;
import de.klarcloudservice.netty.NettySocketClient;
import de.klarcloudservice.netty.channel.ChannelHandler;
import de.klarcloudservice.netty.in.*;
import de.klarcloudservice.netty.packets.PacketOutStartGameServer;
import de.klarcloudservice.netty.packets.PacketOutStartProxy;
import de.klarcloudservice.utility.TypeTokenAdaptor;
import de.klarcloudservice.utility.cloudsystem.InternalCloudNetwork;
import de.klarcloudservice.utility.cloudsystem.KlarCloudAddresses;
import lombok.Getter;
import lombok.Setter;
import net.md_5.bungee.BungeeCord;
import org.apache.commons.lang.Validate;

import javax.management.InstanceAlreadyExistsException;
import java.nio.file.Paths;

/**
 * @author _Klaro | Pasqual K. / created on 01.11.2018
 */

@Setter
@Getter
public class KlarCloudAPIBungee {
    @Getter
    public static KlarCloudAPIBungee instance;

    private final NettySocketClient nettySocketClient;
    private final NettyHandler nettyHandler = new NettyHandler();
    private final ChannelHandler channelHandler = new ChannelHandler();

    private final ProxyStartupInfo proxyStartupInfo;
    private ProxyInfo proxyInfo;
    private InternalCloudNetwork internalCloudNetwork = new InternalCloudNetwork();

    /**
     * Creates a new BungeeCord Plugin instance
     *
     * @throws Throwable
     */
    public KlarCloudAPIBungee() throws Throwable {
        if (instance == null)
            instance = this;
        else
            throw new InstanceAlreadyExistsException();

        KlarCloudLibrary.sendHeader();

        Configuration configuration = Configuration.loadConfiguration(Paths.get("klarcloud/config.json"));

        final KlarCloudAddresses klarCloudAddresses = configuration.getValue("address", TypeTokenAdaptor.getKlarCloudAddressesType());
        new KlarCloudLibraryService(new KlarCloudConsoleLogger(false), configuration.getStringValue("controllerKey"), klarCloudAddresses.getHost(), new EventManager());

        this.proxyStartupInfo = configuration.getValue("startupInfo", TypeTokenAdaptor.getProxyStartupInfoType());
        this.proxyInfo = configuration.getValue("info", TypeTokenAdaptor.getProxyInfoType());

        BungeecordBootstrap.getInstance().getProxy().getPluginManager().registerListener(BungeecordBootstrap.getInstance(), new CloudConnectListener());

        this.nettyHandler.registerHandler("InitializeCloudNetwork", new PacketInInitializeInternal());
        this.nettyHandler.registerHandler("ProcessAdd", new PacketInProcessAdd());
        this.nettyHandler.registerHandler("ProcessRemove", new PacketInProcessRemove());
        this.nettyHandler.registerHandler("UpdateAll", new PacketInUpdateAll());
        this.nettyHandler.registerHandler("ServerInfoUpdate", new PacketInServerInfoUpdate());

        this.nettySocketClient = new NettySocketClient();
        this.nettySocketClient.connect(
                klarCloudAddresses, nettyHandler, channelHandler, configuration.getBooleanValue("ssl"),
                configuration.getStringValue("controllerKey"), this.proxyStartupInfo.getName()
        );
        //configuration.remove("controllerKey").saveAsConfigurationFile(Paths.get("klarcloud/config.json"));
    }

    public void startGameServer(final String serverGroupName) {
        this.startGameServer(serverGroupName, new Configuration());
    }

    public void startGameServer(final String serverGroupName, final Configuration preConfig) {
        final ServerGroup serverGroup = this.internalCloudNetwork.getServerGroups().getOrDefault(serverGroupName, null);

        Validate.notNull(serverGroup, "ServerGroup has to exist");
        this.startGameServer(serverGroup, preConfig);
    }

    public void startGameServer(final ServerGroup serverGroup) {
        this.startGameServer(serverGroup.getName(), new Configuration());
    }

    public void startGameServer(final ServerGroup serverGroup, final Configuration preConfig) {
        this.channelHandler.sendPacketAsynchronous("KlarCloudController", new PacketOutStartGameServer(serverGroup, preConfig));
    }

    public void startProxy(final String proxyGroupName) {
        this.startProxy(proxyGroupName, new Configuration());
    }

    public void startProxy(final String proxyGroupName, final Configuration preConfig) {
        final ProxyGroup proxyGroup = this.internalCloudNetwork.getProxyGroups().getOrDefault(proxyGroupName, null);

        Validate.notNull(proxyGroup, "ProxyGroup has to exist");
        this.channelHandler.sendPacketAsynchronous("KlarCloudController", new PacketOutStartProxy(proxyGroup, preConfig));
    }

    public void startProxy(final ProxyGroup proxyGroup) {
        this.startProxy(proxyGroup.getName());
    }

    public void startProxy(final ProxyGroup proxyGroup, final Configuration preConfig) {
        this.startProxy(proxyGroup.getName(), preConfig);
    }

    /**
     * @deprecated
     */
    @Deprecated
    public void removeInternalProcess() {
        BungeecordBootstrap.getInstance().onDisable();
    }
}
