/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.netty.packets;

import de.klarcloudservice.KlarCloudAPIBungee;
import de.klarcloudservice.configurations.Configuration;
import de.klarcloudservice.netty.packet.Packet;
import de.klarcloudservice.netty.packet.enums.PacketSender;
import de.klarcloudservice.netty.packet.enums.QueryType;

import java.util.Collections;
import java.util.UUID;

/**
 * @author _Klaro | Pasqual K. / created on 07.11.2018
 */

public final class PacketOutCommandExecute extends Packet {
    public PacketOutCommandExecute(final String name, final UUID uuid, final String command) {
        super("CommandExecute", new Configuration().addProperty("uuid", uuid).addStringProperty("command", command).addStringProperty("name", name).addStringProperty("proxyName", KlarCloudAPIBungee.getInstance().getProxyInfo().getCloudProcess().getName()), Collections.singletonList(QueryType.COMPLETE), PacketSender.PROCESS_PROXY);
    }
}
