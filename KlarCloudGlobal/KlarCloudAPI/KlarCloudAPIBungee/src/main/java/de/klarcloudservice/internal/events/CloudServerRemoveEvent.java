/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.internal.events;

import de.klarcloudservice.meta.info.ServerInfo;
import lombok.AllArgsConstructor;
import lombok.Getter;
import net.md_5.bungee.api.plugin.Event;

/**
 * @author _Klaro | Pasqual K. / created on 11.11.2018
 */

@AllArgsConstructor
@Getter
public class CloudServerRemoveEvent extends Event {
    private ServerInfo serverInfo;
}
