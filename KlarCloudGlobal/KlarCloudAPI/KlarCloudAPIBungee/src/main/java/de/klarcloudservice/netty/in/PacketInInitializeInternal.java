/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.netty.in;

import de.klarcloudservice.KlarCloudAPIBungee;
import de.klarcloudservice.configurations.Configuration;
import de.klarcloudservice.meta.enums.ServerModeType;
import de.klarcloudservice.netty.interfaces.NettyAdaptor;
import de.klarcloudservice.netty.packet.Packet;
import de.klarcloudservice.netty.packet.enums.PacketSender;
import de.klarcloudservice.netty.packet.enums.QueryType;
import de.klarcloudservice.utility.TypeTokenAdaptor;
import net.md_5.bungee.api.ProxyServer;

import java.net.InetSocketAddress;
import java.util.Collections;
import java.util.List;

/**
 * @author _Klaro | Pasqual K. / created on 02.11.2018
 */

public class PacketInInitializeInternal implements NettyAdaptor {
    @Override
    public void handle(Configuration configuration, List<QueryType> queryTypes) {
        if (!queryTypes.contains(QueryType.RESULT) || queryTypes.contains(QueryType.NO_RESULT)) return;

        KlarCloudAPIBungee.getInstance().setInternalCloudNetwork(configuration.getValue("networkProperties", TypeTokenAdaptor.getInternalCloudNetworkType()));
        KlarCloudAPIBungee.getInstance().getChannelHandler().sendPacketAsynchronous("KlarCloudController", new Packet(
                "AuthSuccess", new Configuration().addStringProperty("name", KlarCloudAPIBungee.getInstance().getProxyInfo().getCloudProcess().getName()),
                Collections.singletonList(QueryType.COMPLETE), PacketSender.PROCESS_PROXY
        ));
        KlarCloudAPIBungee.getInstance().getInternalCloudNetwork().getServerProcessManager().getAllRegisteredServerProcesses().forEach(
                process -> {
                    ProxyServer.getInstance().getServers().put(
                            process.getCloudProcess().getName(),
                            ProxyServer.getInstance().constructServerInfo(
                                    process.getCloudProcess().getName(),
                                    new InetSocketAddress(process.getHost(), process.getPort()),
                                    "KlarCloudGameServer",
                                    false
                            ));

                    if (process.getServerGroup().getServerModeType().equals(ServerModeType.LOBBY)) {
                        ProxyServer.getInstance().getConfig().getListeners().forEach(listener ->
                                listener.getServerPriority().add(process.getCloudProcess().getName())
                        );
                    }
                }
        );
    }
}
