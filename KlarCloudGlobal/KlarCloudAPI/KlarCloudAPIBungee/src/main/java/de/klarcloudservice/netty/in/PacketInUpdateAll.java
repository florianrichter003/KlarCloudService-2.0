/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.netty.in;

import de.klarcloudservice.KlarCloudAPIBungee;
import de.klarcloudservice.KlarCloudLibraryService;
import de.klarcloudservice.configurations.Configuration;
import de.klarcloudservice.netty.interfaces.NettyAdaptor;
import de.klarcloudservice.netty.packet.enums.QueryType;
import de.klarcloudservice.utility.TypeTokenAdaptor;

import java.util.List;

/**
 * @author _Klaro | Pasqual K. / created on 25.11.2018
 */

public class PacketInUpdateAll implements NettyAdaptor {
    @Override
    public void handle(Configuration configuration, List<QueryType> queryTypes) {
        KlarCloudAPIBungee.getInstance().setInternalCloudNetwork(configuration.getValue("networkProperties", TypeTokenAdaptor.getInternalCloudNetworkType()));
        KlarCloudLibraryService.getInstance().setInternalCloudNetwork(configuration.getValue("networkProperties", TypeTokenAdaptor.getInternalCloudNetworkType()));

        if (KlarCloudAPIBungee.getInstance().getInternalCloudNetwork().getServerProcessManager().getRegisteredProxyByUID(KlarCloudAPIBungee.getInstance().getProxyInfo().getCloudProcess().getProcessUID()) != null)
            KlarCloudAPIBungee.getInstance().setProxyInfo(KlarCloudAPIBungee.getInstance().getInternalCloudNetwork().getServerProcessManager().getRegisteredProxyByUID(KlarCloudAPIBungee.getInstance().getProxyInfo().getCloudProcess().getProcessUID()));
    }
}
