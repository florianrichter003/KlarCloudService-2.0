/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.bootstrap;

import de.klarcloudservice.KlarCloudAPIBungee;
import de.klarcloudservice.KlarCloudLibrary;
import de.klarcloudservice.commands.CommandHub;
import de.klarcloudservice.commands.CommandJumpto;
import de.klarcloudservice.commands.CommandKlarCloud;
import de.klarcloudservice.listener.CloudAddonsListener;
import de.klarcloudservice.listener.CloudProcessListener;
import de.klarcloudservice.listener.CloudProxyPingListener;
import de.klarcloudservice.netty.authentication.enums.AuthenticationType;
import de.klarcloudservice.netty.packets.PacketOutInternalProcessRemove;
import lombok.Getter;
import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.api.plugin.Plugin;

import java.util.Arrays;

/**
 * @author _Klaro | Pasqual K. / created on 01.11.2018
 */

@Getter
public class BungeecordBootstrap extends Plugin {
    @Getter
    public static BungeecordBootstrap instance;

    @Override
    public void onLoad() {
        instance = this;
    }

    @Override
    public void onEnable() {
        this.getProxy().getConfig().getListeners().forEach(listener -> listener.getServerPriority().clear());

        Arrays.asList(
                new CloudProxyPingListener(),
                new CloudProcessListener(),
                new CloudAddonsListener()
        ).forEach(listener -> this.getProxy().getPluginManager().registerListener(this, listener));

        Arrays.asList(
                new CommandJumpto(),
                new CommandHub(),
                new CommandKlarCloud()
        ).forEach(command -> this.getProxy().getPluginManager().registerCommand(this, command));

        /*
         *  Clear the default config servers
         */
        BungeeCord.getInstance().getConfig().getServers().clear();

        try {
            new KlarCloudAPIBungee();
        } catch (Throwable throwable) {
            throwable.printStackTrace();
            this.onDisable();
        }
    }

    @Override
    public void onDisable() {
        KlarCloudAPIBungee.getInstance().getChannelHandler().sendPacketSynchronized("KlarCloudController", new PacketOutInternalProcessRemove(KlarCloudAPIBungee.getInstance().getProxyStartupInfo().getUid(), AuthenticationType.PROXY));
        KlarCloudLibrary.sleep(1000000000);
    }
}
