/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.commands;

import de.klarcloudservice.KlarCloudController;
import de.klarcloudservice.commands.interfaces.Command;
import de.klarcloudservice.commands.interfaces.CommandSender;
import de.klarcloudservice.netty.packets.PacketOutUpdateAll;
import de.klarcloudservice.utility.uuid.UUIDConverter;

import java.util.UUID;

/**
 * @author _Klaro | Pasqual K. / created on 17.12.2018
 */

public final class CommandWhitelist implements Command {
    @Override
    public void executeCommand(CommandSender commandSender, String[] args) {
        if (args.length == 1 && args[0].equalsIgnoreCase("list")) {
            commandSender.sendMessage("KlarCloud Whitelisted players: ");
            KlarCloudController.getInstance().getInternalCloudNetwork().getProxyGroups().values().forEach(e ->
                    e.getProxyConfig().getWhitelist().forEach(player -> commandSender.sendMessage("- " + player))
            );
        } else if (args.length == 3) {
            if (args[0].equalsIgnoreCase("add")) {
                UUID uuidInput = getUuidFromString(args[2]);
                if (uuidInput == null) {
                    commandSender.sendMessage("This is not a UUID.\n");
                    return;
                }
                if (args[1].equalsIgnoreCase("--all")) {
                    KlarCloudController.getInstance().getInternalCloudNetwork().getProxyGroups().values().forEach(group -> {
                        if (!group.getProxyConfig().getWhitelist().contains(uuidInput)) {
                            KlarCloudController.getInstance().getCloudConfiguration().addPlayerToWhitelist(group.getName(), uuidInput);
                            commandSender.sendMessage("Player " + args[2] + " has been added to " + group.getName() + " whitelist");
                        }
                    });
                    KlarCloudController.getInstance().getChannelHandler().sendToAllSynchronized(new PacketOutUpdateAll(KlarCloudController.getInstance().getInternalCloudNetwork()));
                } else if (KlarCloudController.getInstance().getInternalCloudNetwork().getProxyGroups().containsKey(args[1])) {
                    if (!KlarCloudController.getInstance().getInternalCloudNetwork().getProxyGroups().get(args[1]).getProxyConfig().getWhitelist().contains(args[2])) {
                        KlarCloudController.getInstance().getCloudConfiguration().addPlayerToWhitelist(args[1], uuidInput);
                        KlarCloudController.getInstance().getChannelHandler().sendToAllSynchronized(new PacketOutUpdateAll(KlarCloudController.getInstance().getInternalCloudNetwork()));
                        commandSender.sendMessage("Player " + uuidInput + " has been added to " + args[1] + " whitelist");
                    } else {
                        commandSender.sendMessage("Player is already on the whitelist");
                    }
                } else {
                    commandSender.sendMessage("ProxyGroup isn't registered");
                }
            } else if (args[0].equalsIgnoreCase("remove")) {
                UUID uuidInput = getUuidFromString(args[2]);
                if (uuidInput == null) {
                    commandSender.sendMessage("This is not a UUID.\n");
                    return;
                }
                if (args[1].equalsIgnoreCase("--all")) {
                    KlarCloudController.getInstance().getInternalCloudNetwork().getProxyGroups().values().forEach(group -> {
                        if (group.getProxyConfig().getWhitelist().contains(uuidInput)) {
                            KlarCloudController.getInstance().getCloudConfiguration().removePlayerFromWhitelist(group.getName(), uuidInput);
                            commandSender.sendMessage("Player " + uuidInput + " has been removed from " + group.getName() + " whitelist");
                        }
                    });
                } else if (KlarCloudController.getInstance().getInternalCloudNetwork().getProxyGroups().containsKey(args[1])) {
                    if (KlarCloudController.getInstance().getInternalCloudNetwork().getProxyGroups().get(args[1]).getProxyConfig().getWhitelist().contains(args[2])) {
                        KlarCloudController.getInstance().getCloudConfiguration().removePlayerFromWhitelist(args[1], uuidInput);
                        KlarCloudController.getInstance().getChannelHandler().sendToAllSynchronized(new PacketOutUpdateAll(KlarCloudController.getInstance().getInternalCloudNetwork()));
                        commandSender.sendMessage("Player " + uuidInput + " has been removed from " + args[1] + " whitelist");
                    } else {
                        commandSender.sendMessage("Player isn't on the whitelist");
                    }
                } else {
                    commandSender.sendMessage("ProxyGroup isn't registered");
                }
            }
        } else {
            commandSender.sendMessage("whitelist add <proxyGroupName/--all> <name>");
            commandSender.sendMessage("whitelist remove <proxyGroupName/--all> <name>");
            commandSender.sendMessage("whitelist list");
        }
    }

    private UUID getUuidFromString(String rawUuid) {
        try {
            UUID uuidInput;
            if (rawUuid.contains("-")) uuidInput = UUID.fromString(rawUuid);
            else uuidInput = UUIDConverter.toUUID(rawUuid);
            return uuidInput;
        } catch (IllegalArgumentException e) {
            return null;
        }
    }

    @Override
    public final String getPermission() {
        return "klarcloud.command.whitelist";
    }
}
