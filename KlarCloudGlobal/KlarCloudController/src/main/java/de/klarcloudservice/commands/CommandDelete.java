/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.commands;

import de.klarcloudservice.KlarCloudController;
import de.klarcloudservice.commands.interfaces.Command;
import de.klarcloudservice.commands.interfaces.CommandSender;

import java.io.IOException;

/**
 * @author _Klaro | Pasqual K. / created on 16.12.2018
 */

public final class CommandDelete implements Command {
    @Override
    public void executeCommand(CommandSender commandSender, String[] args) {
        if (args.length != 2) {
            commandSender.sendMessage("delete servergroup <name>");
            commandSender.sendMessage("delete proxygroup <name>");
            commandSender.sendMessage("delete client <name>");
            return;
        }

        switch (args[0].toLowerCase()) {
            case "servergroup": {
                if (KlarCloudController.getInstance().getInternalCloudNetwork().getServerGroups().containsKey(args[1])) {
                    try {
                        KlarCloudController.getInstance().getCloudConfiguration().deleteServerGroup(KlarCloudController.getInstance().getInternalCloudNetwork().getServerGroups().get(args[1]));
                    } catch (final IOException ex) {
                        ex.printStackTrace();
                    }
                } else {
                    commandSender.sendMessage("The ServerGroup isn't registered");
                }
                break;
            }
            case "proxygroup": {
                if (KlarCloudController.getInstance().getInternalCloudNetwork().getProxyGroups().containsKey(args[1])) {
                    try {
                        KlarCloudController.getInstance().getCloudConfiguration().deleteProxyGroup(KlarCloudController.getInstance().getInternalCloudNetwork().getProxyGroups().get(args[1]));
                    } catch (final IOException ex) {
                        ex.printStackTrace();
                    }
                } else {
                    commandSender.sendMessage("The ProxyGroup isn't registered");
                }
                break;
            }
            case "client": {
                if (KlarCloudController.getInstance().getInternalCloudNetwork().getClients().containsKey(args[1])) {
                    KlarCloudController.getInstance().getCloudConfiguration().deleteClient(KlarCloudController.getInstance().getInternalCloudNetwork().getClients().get(args[1]));
                } else {
                    commandSender.sendMessage("The Client isn't registered");
                }
                break;
            }
            default: {
                commandSender.sendMessage("delete servergroup <name>");
                commandSender.sendMessage("delete proxygroup <name>");
                commandSender.sendMessage("delete client <name>");
            }
        }
    }

    @Override
    public final String getPermission() {
        return "klarcloud.command.delete";
    }
}
