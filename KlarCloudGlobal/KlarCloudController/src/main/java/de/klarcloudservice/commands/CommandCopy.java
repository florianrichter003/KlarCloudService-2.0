/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.commands;

import de.klarcloudservice.KlarCloudController;
import de.klarcloudservice.commands.interfaces.Command;
import de.klarcloudservice.commands.interfaces.CommandSender;
import de.klarcloudservice.meta.enums.TemplateBackend;
import de.klarcloudservice.meta.info.ProxyInfo;
import de.klarcloudservice.meta.info.ServerInfo;
import de.klarcloudservice.netty.packets.PacketOutCopyServerIntoTemplate;

/**
 * @author _Klaro | Pasqual K. / created on 16.12.2018
 */

public final class CommandCopy implements Command {
    @Override
    public void executeCommand(CommandSender commandSender, String[] args) {
        if (args.length == 1) {
            if (KlarCloudController.getInstance().getInternalCloudNetwork().getServerProcessManager().getRegisteredServerByName(args[0]) != null) {
                final ServerInfo serverInfo = KlarCloudController.getInstance().getInternalCloudNetwork().getServerProcessManager().getRegisteredServerByName(args[0]);
                if (serverInfo.getServerGroup().getTemplate().getTemplateBackend().equals(TemplateBackend.CLIENT)) {
                    KlarCloudController.getInstance().getChannelHandler().sendPacketAsynchronous(serverInfo.getServerGroup().getClient(), new PacketOutCopyServerIntoTemplate(serverInfo.getCloudProcess().getName() + "-" + serverInfo.getCloudProcess().getProcessUID(), "server", serverInfo.getServerGroup().getName()));
                    commandSender.sendMessage("The Client tries to copy the template.");
                } else {
                    commandSender.sendMessage("You can't copy a server if the template backend is not the client.");
                }
            } else if (KlarCloudController.getInstance().getInternalCloudNetwork().getServerProcessManager().getRegisteredProxyByName(args[0]) != null) {
                final ProxyInfo proxyInfo = KlarCloudController.getInstance().getInternalCloudNetwork().getServerProcessManager().getRegisteredProxyByName(args[0]);
                if (proxyInfo.getProxyGroup().getTemplate().getTemplateBackend().equals(TemplateBackend.CLIENT)) {
                    KlarCloudController.getInstance().getChannelHandler().sendPacketAsynchronous(proxyInfo.getProxyGroup().getClient(), new PacketOutCopyServerIntoTemplate(proxyInfo.getCloudProcess().getName() + "-" + proxyInfo.getCloudProcess().getProcessUID(), "proxy", proxyInfo.getProxyGroup().getName()));
                    commandSender.sendMessage("The Client tries to copy the template.");
                } else {
                    commandSender.sendMessage("You can't copy a server if the template backend is not the client.");
                }
            } else {
                commandSender.sendMessage("The server or proxy isn't connected to Controller.");
            }
        } else {
            commandSender.sendMessage("copy <name>");
        }
    }

    @Override
    public final String getPermission() {
        return "klarcloud.command.copy";
    }
}
