/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.commands;

import de.klarcloudservice.KlarCloudController;
import de.klarcloudservice.commands.interfaces.Command;
import de.klarcloudservice.commands.interfaces.CommandSender;
import de.klarcloudservice.utility.StringUtil;

/**
 * @author _Klaro | Pasqual K. / created on 09.12.2018
 */

public final class CommandReload implements Command {
    @Override
    public void executeCommand(CommandSender commandSender, String[] args) {
        if (args.length == 1) {
            try {
                commandSender.sendMessage("Trying to reload the full cloud system....");
                KlarCloudController.getInstance().reloadAll();
                commandSender.sendMessage("Reload done");
            } catch (final Throwable throwable) {
                StringUtil.printError(KlarCloudController.getInstance().getKlarCloudConsoleLogger(), "An error occurred while reloading CloudSystem", throwable);
            }
        } else commandSender.sendMessage("If you really want to reload KlarCloud, please type \"reload\"");

    }

    @Override
    public String getPermission() {
        return "klarcloud.command.reload";
    }
}
