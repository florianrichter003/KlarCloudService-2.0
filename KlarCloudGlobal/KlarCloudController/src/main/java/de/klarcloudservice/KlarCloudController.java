/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice;

import de.klarcloudservice.addons.AddonParallelLoader;
import de.klarcloudservice.commands.*;
import de.klarcloudservice.commands.interfaces.Command;
import de.klarcloudservice.configuration.CloudConfiguration;
import de.klarcloudservice.event.EventManager;
import de.klarcloudservice.event.enums.EventTargetType;
import de.klarcloudservice.event.events.LoadSuccessEvent;
import de.klarcloudservice.exceptions.LoadException;
import de.klarcloudservice.logging.KlarCloudConsoleLogger;
import de.klarcloudservice.netty.NettyHandler;
import de.klarcloudservice.netty.NettySocketServer;
import de.klarcloudservice.netty.channel.ChannelHandler;
import de.klarcloudservice.netty.in.*;
import de.klarcloudservice.netty.interfaces.NettyAdaptor;
import de.klarcloudservice.netty.packets.PacketOutStopProcess;
import de.klarcloudservice.netty.packets.PacketOutUpdateAll;
import de.klarcloudservice.startup.CloudProcessOfferService;
import de.klarcloudservice.utility.StringUtil;
import de.klarcloudservice.utility.cloudsystem.InternalCloudNetwork;
import de.klarcloudservice.utility.runtime.Reload;
import de.klarcloudservice.utility.runtime.Shutdown;
import de.klarcloudservice.utility.signs.SignSelector;
import de.klarcloudservice.utility.signs.netty.in.PacketInRequestSignUpdate;
import de.klarcloudservice.utility.threading.scheduler.Scheduler;
import de.klarcloudservice.versioneering.VersionController;
import de.klarcloudservice.web.KlarCloudInternalWebServer;
import lombok.Getter;
import lombok.Setter;

import javax.management.InstanceAlreadyExistsException;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * @author _Klaro | Pasqual K. / created on 18.10.2018
 */

@Getter
@Setter
public class KlarCloudController implements Shutdown, Reload {
    public static volatile boolean RUNNING = false;

    @Getter
    private static KlarCloudController instance;

    private CommandManager commandManager;
    private KlarCloudConsoleLogger klarCloudConsoleLogger;
    private InternalCloudNetwork internalCloudNetwork = new InternalCloudNetwork();

    private final NettyHandler nettyHandler = new NettyHandler();
    private final ChannelHandler channelHandler = new ChannelHandler();
    private final Scheduler scheduler = new Scheduler(40);
    private final AddonParallelLoader addonParallelLoader = new AddonParallelLoader();
    private final CloudProcessOfferService cloudProcessOfferService = new CloudProcessOfferService();
    private final EventManager eventManager = new EventManager();

    private List<UUID> uuid = new ArrayList<>();

    private SignSelector signSelector;

    private KlarCloudInternalWebServer klarCloudInternalWebServer;
    private final NettySocketServer nettySocketServer;
    private KlarCloudLibraryService klarCloudLibraryService;
    private CloudConfiguration cloudConfiguration;

    private Thread shutdownHook;

    /**
     * Creates a new instance of the KlarCloudController and prepares all needed handlers
     *
     * @see KlarCloudConsoleLogger
     * @see CommandManager
     *
     * @param klarCloudConsoleLogger    Main Cloud logger, will be used everywhere
     * @param commandManager            Main CommandManager to manage all available commands
     * @param ssl                       If this is {@code true} the cloud will use a self-
     *                                  signed certificate
     * @param time                      Startup time for start time
     * @throws Throwable                If an error occurs while starting CloudSystem
     *                                  the error will be thrown here
     */
    public KlarCloudController(KlarCloudConsoleLogger klarCloudConsoleLogger,
                               CommandManager commandManager,
                               boolean ssl, long time) throws Throwable {
        klarCloudConsoleLogger.info("Trying to create new Controller instance...");

        if (instance == null)
            instance = this;
        else
            StringUtil.printError(klarCloudConsoleLogger,
                    "KlarCloudController Instance already exists",
                    new LoadException(new InstanceAlreadyExistsException()));

        this.klarCloudConsoleLogger = klarCloudConsoleLogger;
        this.commandManager = commandManager;

        cloudConfiguration = new CloudConfiguration(klarCloudConsoleLogger.getConsoleReader());

        cloudConfiguration.getClients().forEach(client -> {
            klarCloudConsoleLogger.info("Loading Client [Name=" + client.getName() + "/Address="
                    + client.getIp() + "]...");
            this.internalCloudNetwork.getClients().put(client.getName(), client);
        });
        cloudConfiguration.getServerGroups().forEach(group -> {
            klarCloudConsoleLogger.info("Loading ServerGroup [Name=" + group.getName() + "/Client="
                    + group.getClient() + "]...");
            this.internalCloudNetwork.getServerGroups().put(group.getName(), group);
        });
        cloudConfiguration.getProxyGroups().forEach(proxy -> {
            klarCloudConsoleLogger.info("Loading DefaultProxyGroup [Name=" + proxy.getName() + "/Client="
                    + proxy.getClient() + "]...");
            this.internalCloudNetwork.getProxyGroups().put(proxy.getName(), proxy);
        });

        if (this.internalCloudNetwork.isSigns())
            this.signSelector = new SignSelector();

        this.preparePacketHandlers();
        this.prepareCommands();

        this.klarCloudLibraryService = new KlarCloudLibraryService(klarCloudConsoleLogger,
                this.cloudConfiguration.getControllerKey(), null, eventManager);
        this.klarCloudLibraryService.setInternalCloudNetwork(internalCloudNetwork);

        this.addonParallelLoader.loadAddons();

        this.nettySocketServer = new NettySocketServer(ssl, this.cloudConfiguration.getNettyAddress(),
                !cloudConfiguration.getCertFile().equalsIgnoreCase(StringUtil.NULL) ?
                        new File(cloudConfiguration.getCertFile()) : null,
                !cloudConfiguration.getKeyFile().equalsIgnoreCase(StringUtil.NULL) ?
                        new File(cloudConfiguration.getKeyFile()) : null);

        if (cloudConfiguration.getWebAddress() != null)
            klarCloudInternalWebServer = new KlarCloudInternalWebServer(cloudConfiguration.getWebAddress(), ssl,
                    !cloudConfiguration.getCertFile().equalsIgnoreCase(StringUtil.NULL) ?
                            new File(cloudConfiguration.getCertFile()) : null,
                    !cloudConfiguration.getKeyFile().equalsIgnoreCase(StringUtil.NULL) ?
                            new File(cloudConfiguration.getKeyFile()) : null);

        klarCloudConsoleLogger.info("Trying to startup scheduler thread...");

        Thread thread = new Thread(this.scheduler);
        thread.setDaemon(true);
        thread.start();

        klarCloudConsoleLogger.info("Scheduler thread is now alive");
        klarCloudConsoleLogger.info("Trying to add Runtime-Shutdown-Hook...");

        this.shutdownHook = new Thread(this::shutdownAll, "Shutdown-Hook");
        Runtime.getRuntime().addShutdownHook(this.shutdownHook);

        klarCloudConsoleLogger.info("Shutdown-Hook has been added to Runtime");

        this.scheduler.runTaskRepeatAsync(cloudProcessOfferService, 0, 250);

        this.addonParallelLoader.enableAddons();
        this.checkForUpdates();

        RUNNING = true;

        klarCloudConsoleLogger.info("KlarCloudService has been successfully loaded, took " +
                (System.currentTimeMillis() - time) + "ms");
        this.eventManager.callEvent(EventTargetType.LOAD_SUCCESS, new LoadSuccessEvent(true));
    }

    /**
     * Prepares all packet Handlers
     *
     * @see NettyHandler#registerHandler(String, NettyAdaptor)
     */
    private void preparePacketHandlers() {
        klarCloudConsoleLogger.info("Trying to register Packet-Handlers...");

        this.nettyHandler
                .registerHandler("AuthSuccess", new PacketInAuthSuccess())
                .registerHandler("SendControllerConsoleMessage", new PacketInSendControllerConsoleMessage())
                .registerHandler("ProcessAdd", new PacketInAddProcess())
                .registerHandler("ProcessRemove", new PacketInRemoveProcess())
                .registerHandler("UpdateInternalCloudNetwork", new PacketInUpdateInternalCloudNetwork())
                .registerHandler("InternalProcessRemove", new PacketInRemoveInternalProcess())
                .registerHandler("CommandExecute", new PacketInCommandExecute())
                .registerHandler("ServerInfoUpdate", new PacketInServerInfoUpdate())
                .registerHandler("LoginPlayer", new PacketInLoginPlayer())
                .registerHandler("LogoutPlayer", new PacketInLogoutPlayer())
                .registerHandler("PlayerAccepted", new PacketInPlayerAccepted())
                .registerHandler("DispatchCommandLine", new PacketInDispatchConsoleCommand())
                .registerHandler("StartGameProcess", new PacketInStartGameProcess())
                .registerHandler("RequestSignUpdate", new PacketInRequestSignUpdate())
                .registerHandler("ServerDisable", new PacketInServerDisable())
                .registerHandler("StartProxyProcess", new PacketInStartProxyProcess());

        klarCloudConsoleLogger.info("The following Handlers are registered:");
        nettyHandler.getHandlers().forEach(handler -> klarCloudConsoleLogger.info("    " + handler));
        klarCloudConsoleLogger.emptyLine().info("All Handlers are now registered");
    }

    /**
     * Prepares all commands
     *
     * @see CommandManager#registerCommand(String, Command)
     */
    private void prepareCommands() {
        klarCloudConsoleLogger.info("Trying to register CloudCommands...");

        this.commandManager
                .registerCommand("exit", new CommandExit())
                .registerCommand("help", new CommandHelp())
                .registerCommand("execute", new CommandExecute())
                .registerCommand("process", new CommandProcess())
                .registerCommand("reload", new CommandReload())
                .registerCommand("create", new CommandCreate())
                .registerCommand("delete", new CommandDelete())
                .registerCommand("clear", new CommandClear())
                .registerCommand("info", new CommandInfo())
                .registerCommand("copy", new CommandCopy())
                .registerCommand("update", new CommandUpdate())
                .registerCommand("whitelist", new CommandWhitelist());

        klarCloudConsoleLogger.info("The following Commands are registered:");
        commandManager.getCommands().forEach(command -> klarCloudConsoleLogger.info("   \"" + command + "\""));
        klarCloudConsoleLogger.emptyLine().info("All Commands are now registered");
    }

    /**
     * Reloads the KlarCloud Controller
     *
     * @throws Throwable
     */
    @Override
    public void reloadAll() throws Throwable {
        this.klarCloudConsoleLogger.info("Trying to reload the CloudSystem, this may take a long time...");

        this.cloudConfiguration = null;
        this.nettyHandler.clearHandlers();
        this.commandManager.clearCommands();

        this.internalCloudNetwork.getServerGroups().clear();
        this.internalCloudNetwork.getProxyGroups().clear();
        this.internalCloudNetwork.getClients().clear();

        this.addonParallelLoader.disableAddons();
        this.eventManager.unregisterAllListener();

        if (this.internalCloudNetwork.isSigns())
            this.signSelector.shutdownAll();

        this.signSelector = null;

        Runtime.getRuntime().removeShutdownHook(this.shutdownHook);
        this.shutdownHook = null;

        this.cloudConfiguration = new CloudConfiguration(klarCloudConsoleLogger.getConsoleReader());
        cloudConfiguration.getClients().forEach(client -> {
            klarCloudConsoleLogger.info("Loading Client [Name=" + client.getName() + "/Address=" + client.getIp()
                    + "]...");
            this.internalCloudNetwork.getClients().put(client.getName(), client);
        });
        cloudConfiguration.getServerGroups().forEach(group -> {
            klarCloudConsoleLogger.info("Loading ServerGroup [Name=" + group.getName() + "/Client=" + group.getClient()
                    + "]...");
            this.internalCloudNetwork.getServerGroups().put(group.getName(), group);
        });
        cloudConfiguration.getProxyGroups().forEach(proxy -> {
            klarCloudConsoleLogger.info("Loading ProxyGroup [Name=" + proxy.getName() + "/Client=" + proxy.getClient()
                    + "]...");
            this.internalCloudNetwork.getProxyGroups().put(proxy.getName(), proxy);
        });

        this.addonParallelLoader.loadAddons();

        if (this.internalCloudNetwork.isSigns())
            this.signSelector = new SignSelector();

        this.preparePacketHandlers();
        this.prepareCommands();

        this.shutdownHook = new Thread(this::shutdownAll, "Shutdown-Hook");
        Runtime.getRuntime().addShutdownHook(this.shutdownHook);

        this.channelHandler.sendToAllSynchronized(new PacketOutUpdateAll(this.internalCloudNetwork));
        KlarCloudLibraryService.getInstance().setInternalCloudNetwork(this.internalCloudNetwork);

        this.addonParallelLoader.enableAddons();
        this.checkForUpdates();

        this.klarCloudConsoleLogger.info("Reloading was completed successfully.");
    }

    /**
     * Shutdowns the Controller
     */
    @Override
    public void shutdownAll() {
        RUNNING = false;

        this.internalCloudNetwork.getServerProcessManager().getAllRegisteredServerProcesses().forEach(e -> {
            this.klarCloudConsoleLogger.info("Stopping ServerProcess " + e.getCloudProcess().getName() + "...");
            this.channelHandler.sendPacketSynchronized(e.getCloudProcess().getClient(),
                    new PacketOutStopProcess(e.getCloudProcess().getName()));
            this.klarCloudConsoleLogger.info("ServerProcess " + e.getCloudProcess().getName() + " was stopped");
        });

        this.internalCloudNetwork.getServerProcessManager().getAllRegisteredProxyProcesses().forEach(e -> {
            this.klarCloudConsoleLogger.info("Stopping ProxyProcess " + e.getCloudProcess().getName() + "...");
            this.channelHandler.sendPacketSynchronized(e.getCloudProcess().getClient(),
                    new PacketOutStopProcess(e.getCloudProcess().getName()));
            this.klarCloudConsoleLogger.info("ProxyProcess " + e.getCloudProcess().getName() + " was stopped");
        });

        this.klarCloudConsoleLogger.info("Waiting for tasks to close...");
        KlarCloudLibrary.sleep(1000);

        if (this.klarCloudInternalWebServer != null)
            this.klarCloudInternalWebServer.shutdown();

        this.nettySocketServer.close();

        KlarCloudLibrary.end();
    }

    public void checkForUpdates() {
        if (VersionController.isVersionAvailable()) {
            klarCloudConsoleLogger.warn("A newer version of KlarCloud is available");
            klarCloudConsoleLogger.warn("Type \"update\" to get more information");
        } else {
            klarCloudConsoleLogger.info("Your version is up-to-date");
        }
    }
}
