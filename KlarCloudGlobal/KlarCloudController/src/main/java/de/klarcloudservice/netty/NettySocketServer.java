/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.netty;

import de.klarcloudservice.KlarCloudController;
import de.klarcloudservice.KlarCloudLibrary;
import de.klarcloudservice.KlarCloudLibraryService;
import de.klarcloudservice.netty.channel.ChannelHandler;
import de.klarcloudservice.utility.AccessChecker;
import de.klarcloudservice.utility.cloudsystem.KlarCloudAddresses;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.buffer.ByteBufAllocator;
import io.netty.channel.*;
import io.netty.handler.ssl.SslContext;
import io.netty.handler.ssl.SslContextBuilder;
import io.netty.handler.ssl.util.SelfSignedCertificate;
import lombok.Getter;

import java.io.File;
import java.net.InetSocketAddress;
import java.security.PrivateKey;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;

/**
 * @author _Klaro | Pasqual K. / created on 21.10.2018
 */

@Getter
public class NettySocketServer extends ChannelInitializer<Channel> implements AutoCloseable {
    private SslContext sslContext;
    private EventLoopGroup workerGroup = KlarCloudLibrary.eventLoopGroup(), bossGroup = KlarCloudLibrary.eventLoopGroup();

    /**
     * Prepares a socket server by using io.netty and binds him using
     * {@link ServerBootstrap} synchronized on the main thread. This
     * is blocking and will await the bind. If ssl is enabled the
     * {@link SslContext} will be prepared first by using an self-
     * signed certificate using an SslContextBuilder
     * {@link SslContextBuilder#forServer(PrivateKey, X509Certificate...)}.
     * Please note all {@link ChannelOption}'s below in the main source.
     * If an handler connects the cloud will prepare the channel by using
     * {@link KlarCloudLibrary#prepareChannel(Channel, NettyHandler, ChannelHandler)}.
     * If an exceptions occurs it will be catch by {@link Throwable} below and
     * printed in the console. If an error occurs, you can contact the support.
     *
     * @see io.netty.bootstrap.Bootstrap#bind(String, int)
     * @see ChannelInitializer<Channel>
     * @see KlarCloudLibrary#prepareChannel(Channel, NettyHandler, ChannelHandler)
     *
     * @since 2.0
     *
     * @param ssl                   If this is {@code true} the ssl context
     *                              will be enabled and a self-signed certificate
     *                              will be added to every channel, which tries to
     *                              connect to the KlarCloudController
     * @param klarCloudAddresses    Main address where the cloud tries to bind
     *                              the socket server to. Please make sure that
     *                              the port is not in use, yet
     */
    public NettySocketServer(boolean ssl, KlarCloudAddresses klarCloudAddresses, File cert, File key) {
        try {
            if (ssl) {
                if (cert == null || key == null) {
                    KlarCloudLibraryService.getInstance().getKlarCloudConsoleLogger().info("Trying to create SelfSignedCertificate...");
                    SelfSignedCertificate selfSignedCertificate = new SelfSignedCertificate();
                    sslContext = SslContextBuilder.forServer(selfSignedCertificate.key(), selfSignedCertificate.cert()).build();
                    KlarCloudLibraryService.getInstance().getKlarCloudConsoleLogger().info("SelfSignedCertificate has been created");
                } else {
                    KlarCloudLibraryService.getInstance().getKlarCloudConsoleLogger().info("Trying to load SSL certificate...");
                    sslContext = SslContextBuilder.forServer(cert, key).build();
                    KlarCloudLibraryService.getInstance().getKlarCloudConsoleLogger().info("SSL certificate has been loaded");
                }
            }

            ServerBootstrap serverBootstrap = new ServerBootstrap()
                    .group(bossGroup, workerGroup)

                    .childOption(ChannelOption.SO_KEEPALIVE, true)
                    .childOption(ChannelOption.IP_TOS, 24)
                    .childOption(ChannelOption.TCP_NODELAY, true)
                    .childOption(ChannelOption.ALLOCATOR, ByteBufAllocator.DEFAULT)
                    .childOption(ChannelOption.AUTO_READ, true)
                    .childHandler(this)
                    .option(ChannelOption.ALLOCATOR, ByteBufAllocator.DEFAULT)
                    .option(ChannelOption.AUTO_READ, true)

                    .channel(KlarCloudLibrary.klarCloudServerSocketChanel());

            KlarCloudController.getInstance().getKlarCloudConsoleLogger().info("Using " + (KlarCloudLibrary.EPOLL ? "Epoll transport type." : "Nio transport type"));
            KlarCloudController.getInstance().getKlarCloudConsoleLogger().info("Trying to bind @" + klarCloudAddresses.getHost() + ":" + klarCloudAddresses.getPort());

            ChannelFuture channelFuture = serverBootstrap.bind(klarCloudAddresses.getHost(), klarCloudAddresses.getPort())
                    .addListener((handler) -> {
                        if (handler.isSuccess())
                            KlarCloudController.getInstance().getKlarCloudConsoleLogger().info("KlarCloudService is now ready and listening on " + klarCloudAddresses.getHost() + ":" + klarCloudAddresses.getPort());
                        else
                            KlarCloudController.getInstance().getKlarCloudConsoleLogger().err("KlarCloudService could not bind on " + klarCloudAddresses.getHost() + ":" + klarCloudAddresses.getPort());
                    }).addListener(ChannelFutureListener.CLOSE_ON_FAILURE).addListener(ChannelFutureListener.FIRE_EXCEPTION_ON_FAILURE);

            channelFuture.sync().channel().closeFuture();
        } catch (final Throwable throwable) {
            throwable.printStackTrace();
        }
    }

    @Override
    protected void initChannel(Channel channel) {
        final InetSocketAddress inetSocketAddress = ((InetSocketAddress) channel.remoteAddress());

        KlarCloudController.getInstance().getKlarCloudConsoleLogger().info("Channel [Address=" + inetSocketAddress.getAddress().getHostAddress() + "/Port="
                + inetSocketAddress.getPort() + "] connecting...");

        if (this.isIpAllowed(inetSocketAddress.getAddress().getHostAddress())) {
            KlarCloudController.getInstance().getKlarCloudConsoleLogger().info("Initializing channel [Address=" + inetSocketAddress.getAddress().getHostAddress() + "/Port="
                    + inetSocketAddress.getPort() + "]");
            if (sslContext != null)
                channel.pipeline().addLast(sslContext.newHandler(channel.alloc()));

            KlarCloudLibrary.prepareChannel(channel, KlarCloudController.getInstance().getNettyHandler(), KlarCloudController.getInstance().getChannelHandler());
            KlarCloudController.getInstance().getKlarCloudConsoleLogger().info("Channel [Address=" + inetSocketAddress.getAddress().getHostAddress() + "/Port="
                    + inetSocketAddress.getPort() + "] connected.");
        } else {
            channel.close().addListener(ChannelFutureListener.CLOSE_ON_FAILURE).addListener(ChannelFutureListener.FIRE_EXCEPTION_ON_FAILURE);
            KlarCloudController.getInstance().getKlarCloudConsoleLogger().info("Channel [Address=" + inetSocketAddress.getAddress().getHostAddress() + "/Port="
                    + inetSocketAddress.getPort() + "] got disconnected.");
        }
    }

    @Override
    public void close() {
        workerGroup.shutdownGracefully();
        bossGroup.shutdownGracefully();
    }

    private boolean isIpAllowed(String ip) {
        List<String> ips = new ArrayList<>();

        KlarCloudController.getInstance().getInternalCloudNetwork().getClients().values().forEach(client -> ips.add(client.getIp()));
        KlarCloudController.getInstance().getInternalCloudNetwork().getProxyGroups().values().forEach(proxy -> ips.add(proxy.getProxyConfig().getIp()));
        KlarCloudController.getInstance().getInternalCloudNetwork().getServerGroups().values().forEach(group -> ips.add(KlarCloudController.getInstance().getInternalCloudNetwork().getClients().get(group.getClient()).getIp()));

        for (String string : ips)
            if (new AccessChecker().checkString(string, ip).isAccepted())
                return true;

        return false;
    }
}
