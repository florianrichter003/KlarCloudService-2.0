/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.netty.in;

import de.klarcloudservice.KlarCloudController;
import de.klarcloudservice.configurations.Configuration;
import de.klarcloudservice.netty.interfaces.NettyAdaptor;
import de.klarcloudservice.netty.packet.Packet;
import de.klarcloudservice.netty.packet.enums.PacketSender;
import de.klarcloudservice.netty.packet.enums.QueryType;

import java.util.Collections;
import java.util.List;
import java.util.UUID;

/**
 * @author _Klaro | Pasqual K. / created on 14.12.2018
 */

public class PacketInPlayerAccepted implements NettyAdaptor {
    @Override
    public void handle(Configuration configuration, List<QueryType> queryTypes) {
        KlarCloudController.getInstance().getChannelHandler().sendPacketSynchronized(configuration.getStringValue("name"),
                new Packet("PlayerAccepted", new Configuration().addBooleanProperty("accepted", KlarCloudController.getInstance().getUuid().contains(configuration.getValue("uuid", UUID.class))).addProperty("uuid", configuration.getValue("uuid", UUID.class)), Collections.singletonList(QueryType.COMPLETE), PacketSender.PROCESS_SERVER));
    }
}
