/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.configuration;

import com.google.gson.reflect.TypeToken;
import de.klarcloudservice.KlarCloudController;
import de.klarcloudservice.KlarCloudLibrary;
import de.klarcloudservice.KlarCloudLibraryService;
import de.klarcloudservice.configurations.Configuration;
import de.klarcloudservice.meta.client.Client;
import de.klarcloudservice.meta.proxy.ProxyGroup;
import de.klarcloudservice.meta.proxy.defaults.DefaultProxyGroup;
import de.klarcloudservice.meta.server.ServerGroup;
import de.klarcloudservice.meta.server.defaults.LobbyGroup;
import de.klarcloudservice.meta.web.WebUser;
import de.klarcloudservice.netty.packets.PacketOutStopProcess;
import de.klarcloudservice.netty.packets.PacketOutUpdateAll;
import de.klarcloudservice.utility.StringUtil;
import de.klarcloudservice.utility.TypeTokenAdaptor;
import de.klarcloudservice.utility.cloudsystem.KlarCloudAddresses;
import jline.console.ConsoleReader;
import lombok.Getter;

import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

/**
 * @author _Klaro | Pasqual K. / created on 21.10.2018
 */

@Getter
public class CloudConfiguration {
    private KlarCloudAddresses nettyAddress;
    private KlarCloudAddresses webAddress;
    private boolean autoUpdate;
    private String controllerKey, host, splitter, certFile, keyFile;
    private List<Client> clients;
    private List<ServerGroup> serverGroups = new ArrayList<>();
    private List<de.klarcloudservice.meta.proxy.ProxyGroup> proxyGroups = new ArrayList<>();
    private List<WebUser> webUsers = new ArrayList<>();

    /**
     * Prepares and loads KlarCloudController Configuration
     *
     * @throws Throwable
     */
    public CloudConfiguration(ConsoleReader consoleReader) throws Throwable {
        for (File directory : new File[]{
                new File("klarcloud/groups/proxies"),
                new File("klarcloud/groups/servers"),
                new File("klarcloud/cache"),
                new File("klarcloud/addons")
        })
            directory.mkdirs();

        KlarCloudController.getInstance().getKlarCloudConsoleLogger().info("Trying to load KarCloud-Controller Configuration...");

        if (this.init() && this.initCloud())
            KlarCloudController.getInstance().getKlarCloudConsoleLogger().info("Default KlarCloud-Controller Configuration has been successfully created");

        this.load();
        this.loadMessages();
        this.loadProxyGroups();
        this.loadServerGroups();
    }

    private boolean init() {
        if (Files.exists(Paths.get("configuration.properties"))) return false;

        final String ip = KlarCloudLibrary.getInternalAddress();

        Properties properties = new Properties();

        properties.setProperty("server.ip", ip);
        properties.setProperty("server.port", 5000 + StringUtil.EMPTY);

        properties.setProperty("webServer.enabled", true + StringUtil.EMPTY);
        properties.setProperty("webServer.ip", ip);
        properties.setProperty("webServer.port", 4790 + StringUtil.EMPTY);

        properties.setProperty("ssl.certFilePath", StringUtil.NULL);
        properties.setProperty("ssl.keyFilePath", StringUtil.NULL);

        properties.setProperty("signs.enabled", String.valueOf(true));

        properties.setProperty("general.server-splitter", "-");

        KlarCloudController.getInstance().getKlarCloudConsoleLogger().info("Generating your new Controller Key...");
        final String key = KlarCloudLibrary.THREAD_LOCAL_RANDOM.nextLong(0, Long.MAX_VALUE) + StringUtil.EMPTY + KlarCloudLibrary.THREAD_LOCAL_RANDOM.nextLong(0, Long.MAX_VALUE) + StringUtil.EMPTY;
        KlarCloudController.getInstance().getKlarCloudConsoleLogger().info("Your new Controller Key is: ");
        KlarCloudController.getInstance().getKlarCloudConsoleLogger().info("    " + key);
        KlarCloudController.getInstance().getKlarCloudConsoleLogger().emptyLine();

        properties.setProperty("controller.key", key);

        try (OutputStream outputStream = Files.newOutputStream(Paths.get("configuration.properties"))) {
            properties.store(outputStream, "KlarCloud default Configuration");
        } catch (final IOException ex) {
            ex.printStackTrace();
            return false;
        }
        return true;
    }

    private boolean initCloud() {
        if (Files.exists(Paths.get("klarcloud/clients.json"))) return false;

        final String ip = KlarCloudLibrary.getInternalAddress();

        new Configuration().addProperty("client", Collections.singletonList(new Client("Client-01", ip))).saveAsConfigurationFile(Paths.get("klarcloud/clients.json"));
        new Configuration().addProperty("group", new LobbyGroup()).saveAsConfigurationFile(Paths.get("klarcloud/groups/servers/Lobby.json"));
        new Configuration().addProperty("group", new DefaultProxyGroup(ip)).saveAsConfigurationFile(Paths.get("klarcloud/groups/proxies/Proxy.json"));
        new Configuration().addProperty("users",
                Collections.singletonList(new WebUser("administrator",
                        KlarCloudLibrary.THREAD_LOCAL_RANDOM
                                .nextLong(0, Long.MAX_VALUE)
                                + StringUtil.EMPTY
                                + KlarCloudLibrary.THREAD_LOCAL_RANDOM.nextLong(0, Long.MAX_VALUE),
                        KlarCloudLibrary.concurrentHashMap()))).saveAsConfigurationFile(Paths.get("klarcloud/users.json"));

        return true;
    }

    private void load() {
        Properties properties = new Properties();
        try (InputStreamReader inputStreamReader = new InputStreamReader(Files.newInputStream(Paths.get("configuration.properties")))) {
            properties.load(inputStreamReader);
        } catch (final IOException ex) {
            ex.printStackTrace();
        }

        this.host = properties.getProperty("server.ip");
        this.nettyAddress = new KlarCloudAddresses(this.host, Integer.parseInt(properties.getProperty("server.port")));

        this.splitter = properties.getProperty("general.server-splitter");

        if (Boolean.parseBoolean(properties.getProperty("webServer.enabled"))) {
            this.webAddress = new KlarCloudAddresses(properties.getProperty("webServer.ip"), Integer.parseInt(properties.getProperty("webServer.port")));
        }
        this.certFile = properties.getProperty("ssl.certFilePath", StringUtil.NULL);
        this.keyFile = properties.getProperty("ssl.keyFilePath", StringUtil.NULL);
        this.controllerKey = properties.getProperty("controller.key");
        this.autoUpdate = Boolean.parseBoolean(properties.getProperty("general.auto-update"));

        KlarCloudController.getInstance().getInternalCloudNetwork().setSigns(Boolean.parseBoolean(properties.getProperty("signs.enabled")));

        de.klarcloudservice.configurations.Configuration configuration = de.klarcloudservice.configurations.Configuration.loadConfiguration(Paths.get("klarcloud/clients.json"));
        this.clients = configuration.getValue("client", new TypeToken<List<Client>>() {
        }.getType());
        this.webUsers = Configuration.loadConfiguration(Paths.get("klarcloud/users.json")).getValue("users", new TypeToken<List<WebUser>>() {
        }.getType());
    }

    private void loadServerGroups() {
        File dir = new File("klarcloud/groups/servers");
        de.klarcloudservice.configurations.Configuration configuration;

        if (dir.isDirectory()) {
            for (File file : dir.listFiles()) {
                if (file.getName().endsWith(".json")) {
                    try {
                        configuration = de.klarcloudservice.configurations.Configuration.loadConfiguration(file);
                        ServerGroup serverGroup = configuration.getValue("group", TypeTokenAdaptor.getServerGroupType());
                        serverGroups.add(serverGroup);
                    } catch (final Throwable throwable) {
                        throwable.printStackTrace();
                        KlarCloudController.getInstance().getKlarCloudConsoleLogger().err("Failed to load ServerGroup " + file.getName() + "!");
                    }
                }
            }
        }
    }

    private void loadProxyGroups() {
        File dir = new File("klarcloud/groups/proxies");
        de.klarcloudservice.configurations.Configuration configuration;

        if (dir.isDirectory()) {
            for (File file : dir.listFiles()) {
                if (file.getName().endsWith(".json")) {
                    try {
                        configuration = de.klarcloudservice.configurations.Configuration.loadConfiguration(file);
                        de.klarcloudservice.meta.proxy.ProxyGroup proxyGroup = configuration.getValue("group", TypeTokenAdaptor.getProxyGroupType());
                        proxyGroups.add(proxyGroup);
                    } catch (final Throwable throwable) {
                        throwable.printStackTrace();
                        KlarCloudController.getInstance().getKlarCloudConsoleLogger().err("Failed to load DefaultProxyGroup " + file.getName() + "!");
                    }
                }
            }
        }
    }

    private void loadMessages() {
        String messagePath = "klarcloud/messages.json";
        if (!Files.exists(Paths.get(messagePath))) {
            new Configuration()
                    .addStringProperty("internal-global-prefix", "§4K§clar§4C§cloud | ")

                    .addStringProperty("internal-api-bungee-command-no-permission", "§cYou do not have permission to execute this command")

                    .addStringProperty("internal-api-bungee-command-hub-already", "%prefix% §7You are already connected to a hub server")
                    .addStringProperty("internal-api-bungee-command-hub-not-available", "%prefix% §7There is now hub server available")

                    .addStringProperty("internal-api-bungee-command-jumpto-server-player-not-found", "%prefix% §cCould not find player or server to go to")
                    .addStringProperty("internal-api-bungee-command-jumpto-success", "%prefix% §aYou have been connected to server")

                    .addStringProperty("internal-api-bungee-command-klarcloud-invalid-syntax", "%prefix% §7/klarcloud <command>")
                    .addStringProperty("internal-api-bungee-command-klarcloud-no-permission", "%prefix% §7Command not allowed")
                    .addStringProperty("internal-api-bungee-command-klarcloud-command-success", "%prefix% §7Command has been executed successfully \n §7Please check the Controller Console for more details")

                    .addStringProperty("internal-api-bungee-maintenance-join-no-permission", "§cWhitelist is enabled, but you are not added")
                    .addStringProperty("internal-api-bungee-connect-hub-no-server", "%prefix% §7There is no hub server available")

                    .addStringProperty("internal-api-bungee-startup-server", "%prefix% §7ServerProcess §6%server-name% §7is starting...")
                    .addStringProperty("internal-api-bungee-startup-proxy", "%prefix% §7ProxyProcess §6%proxy-name% §7is starting...")
                    .addStringProperty("internal-api-bungee-remove-server", "%prefix% §7ServerProcess §6%server-name% §7is stopping...")
                    .addStringProperty("internal-api-bungee-remove-proxy", "%prefix% §7ProxyProcess §6%proxy-name% §7is stopping...")

                    .addStringProperty("internal-api-spigot-connect-no-permission", "%prefix% §cYou do not have permission to join this server")
                    .addStringProperty("internal-api-spigot-connect-only-proxy", "%prefix% §cOnly Proxy join allowed")

                    .addStringProperty("internal-api-spigot-command-signs-not-enabled", "%prefix% §7Signs aren't enabled")
                    .addStringProperty("internal-api-spigot-command-signs-create-usage", "%prefix% §7/selectors selector signs new <group>")
                    .addStringProperty("internal-api-spigot-command-signs-create-success", "%prefix% §7Sign was created successfully")
                    .addStringProperty("internal-api-spigot-command-signs-create-already-exists", "%prefix% §7Sign already exits")
                    .addStringProperty("internal-api-spigot-command-signs-block-not-sign", "%prefix% §7Target block isn't a sign")
                    .addStringProperty("internal-api-spigot-command-signs-delete-success", "%prefix% §7Sign was deleted")
                    .addStringProperty("internal-api-spigot-command-signs-item-success", "%prefix% §7The Sign-Item was added to your Inventory")
                    .addStringProperty("internal-api-spigot-command-signs-delete-not-exists", "%prefix% §7Sign doesn't exits")
                    .addStringProperty("internal-api-spigot-command-signs-list", "%prefix% §7The Following Signs are registered:")
                    .addStringProperty("internal-api-spigot-command-signs-usage-1", "%prefix% §7/selectors <help/selector> <signs> <new> <group>")
                    .addStringProperty("internal-api-spigot-command-signs-usage-2", "%prefix% §7/selectors <help/selector> <signs> <remove/group-name>")
                    .addStringProperty("internal-api-spigot-command-signs-usage-3", "%prefix% §7/selectors <selector> <signs> <list>")

                    .saveAsConfigurationFile(Paths.get(messagePath));
        }

        KlarCloudController.getInstance().getInternalCloudNetwork().setMessages(Configuration.loadConfiguration(Paths.get(messagePath)));
        KlarCloudController.getInstance().getInternalCloudNetwork().setPrefix(KlarCloudController.getInstance().getInternalCloudNetwork().getMessages().getStringValue("internal-global-prefix"));
    }

    public void createServerGroup(final ServerGroup serverGroup) {
        new Configuration().addProperty("group", serverGroup).saveAsConfigurationFile(Paths.get("klarcloud/groups/servers/" + serverGroup.getName() + ".json"));
        KlarCloudController.getInstance().getKlarCloudConsoleLogger().info("Loading ServerGroup [Name=" + serverGroup.getName() + "/Client=" + serverGroup.getClient() + "]...");
        KlarCloudController.getInstance().getInternalCloudNetwork().getServerGroups().put(serverGroup.getName(), serverGroup);
        KlarCloudLibraryService.getInstance().setInternalCloudNetwork(KlarCloudController.getInstance().getInternalCloudNetwork());

        KlarCloudController.getInstance().getChannelHandler().sendToAllSynchronized(new PacketOutUpdateAll(KlarCloudController.getInstance().getInternalCloudNetwork()));
    }

    public void createProxyGroup(final ProxyGroup proxyGroup) {
        new Configuration().addProperty("group", proxyGroup).saveAsConfigurationFile(Paths.get("klarcloud/groups/proxies/" + proxyGroup.getName() + ".json"));
        KlarCloudController.getInstance().getKlarCloudConsoleLogger().info("Loading ProxyGroup [Name=" + proxyGroup.getName() + "/Client=" + proxyGroup.getClient() + "]...");
        KlarCloudController.getInstance().getInternalCloudNetwork().getProxyGroups().put(proxyGroup.getName(), proxyGroup);
        KlarCloudLibraryService.getInstance().setInternalCloudNetwork(KlarCloudController.getInstance().getInternalCloudNetwork());

        KlarCloudController.getInstance().getChannelHandler().sendToAllSynchronized(new PacketOutUpdateAll(KlarCloudController.getInstance().getInternalCloudNetwork()));
    }

    public void createClient(final Client client) {
        Configuration configuration = Configuration.loadConfiguration(Paths.get("klarcloud/clients.json"));

        List<Client> clients = configuration.getValue("client", new TypeToken<List<Client>>() {
        }.getType());
        clients.add(client);
        configuration.addProperty("client", clients).saveAsConfigurationFile(Paths.get("klarcloud/clients.json"));

        KlarCloudController.getInstance().getKlarCloudConsoleLogger().info("Loading Client [Name=" + client.getName() + "/Address=" + client.getIp() + "]...");
        KlarCloudController.getInstance().getInternalCloudNetwork().getClients().put(client.getName(), client);
        KlarCloudLibraryService.getInstance().setInternalCloudNetwork(KlarCloudController.getInstance().getInternalCloudNetwork());

        KlarCloudController.getInstance().getChannelHandler().sendToAllSynchronized(new PacketOutUpdateAll(KlarCloudController.getInstance().getInternalCloudNetwork()));
    }

    public void deleteServerGroup(final ServerGroup serverGroup) throws IOException {
        Files.delete(Paths.get("klarcloud/groups/servers/" + serverGroup.getName() + ".json"));

        KlarCloudController.getInstance().getKlarCloudConsoleLogger().info("Deleting ServerGroup [Name=" + serverGroup.getName() + "/Client=" + serverGroup.getClient() + "]...");
        KlarCloudController.getInstance().getInternalCloudNetwork().getServerGroups().remove(serverGroup.getName());
        KlarCloudLibraryService.getInstance().setInternalCloudNetwork(KlarCloudController.getInstance().getInternalCloudNetwork());

        KlarCloudController.getInstance().getChannelHandler().sendToAllSynchronized(new PacketOutUpdateAll(KlarCloudController.getInstance().getInternalCloudNetwork()));
        KlarCloudController.getInstance().getInternalCloudNetwork().getServerProcessManager().getAllRegisteredServerProcesses().stream()
                .filter(e -> e.getServerGroup().getName().equals(serverGroup.getName()))
                .forEach(e -> {
                    KlarCloudController.getInstance().getChannelHandler().sendPacketAsynchronous(serverGroup.getClient(), new PacketOutStopProcess(e.getCloudProcess().getName()));
                    KlarCloudLibrary.sleep(2000);
                });
    }

    public void deleteProxyGroup(final ProxyGroup proxyGroup) throws IOException {
        Files.delete(Paths.get("klarcloud/groups/proxies/" + proxyGroup.getName() + ".json"));

        KlarCloudController.getInstance().getKlarCloudConsoleLogger().info("Deleting ProxyGroup [Name=" + proxyGroup.getName() + "/Client=" + proxyGroup.getClient() + "]...");
        KlarCloudController.getInstance().getInternalCloudNetwork().getProxyGroups().remove(proxyGroup.getName());
        KlarCloudLibraryService.getInstance().setInternalCloudNetwork(KlarCloudController.getInstance().getInternalCloudNetwork());

        KlarCloudController.getInstance().getChannelHandler().sendToAllSynchronized(new PacketOutUpdateAll(KlarCloudController.getInstance().getInternalCloudNetwork()));
        KlarCloudController.getInstance().getInternalCloudNetwork().getServerProcessManager().getAllRegisteredProxyProcesses().stream()
                .filter(e -> e.getProxyGroup().getName().equals(proxyGroup.getName()))
                .forEach(e -> {
                    KlarCloudController.getInstance().getChannelHandler().sendPacketAsynchronous(proxyGroup.getClient(), new PacketOutStopProcess(e.getCloudProcess().getName()));
                    KlarCloudLibrary.sleep(2000);
                });
    }

    public void deleteClient(final Client client) {
        Configuration configuration = Configuration.loadConfiguration(Paths.get("klarcloud/clients.json"));

        List<Client> clients = configuration.getValue("client", new TypeToken<List<Client>>() {
        }.getType());
        clients.remove(client);
        configuration.addProperty("client", clients).saveAsConfigurationFile(Paths.get("klarcloud/clients.json"));

        KlarCloudController.getInstance().getKlarCloudConsoleLogger().info("Deleting Client [Name=" + client.getName() + "/Address=" + client.getIp() + "]...");
        KlarCloudController.getInstance().getInternalCloudNetwork().getClients().remove(client.getName());
        KlarCloudLibraryService.getInstance().setInternalCloudNetwork(KlarCloudController.getInstance().getInternalCloudNetwork());

        KlarCloudController.getInstance().getChannelHandler().sendToAllSynchronized(new PacketOutUpdateAll(KlarCloudController.getInstance().getInternalCloudNetwork()));
    }

    public void addPlayerToWhitelist(final String group, UUID playerUuid) {
        ProxyGroup proxyGroup = KlarCloudController.getInstance().getInternalCloudNetwork().getProxyGroups().get(group);
        proxyGroup.getProxyConfig().getWhitelist().add(playerUuid);
        Configuration.loadConfiguration(Paths.get("klarcloud/groups/proxies/" + group + ".json")).addProperty("group", proxyGroup).saveAsConfigurationFile(Paths.get("klarcloud/groups/proxies/" + group + ".json"));
        //KlarCloudController.getInstance().getInternalCloudNetwork().getProxyGroups().remove(group);


        //KlarCloudController.getInstance().getInternalCloudNetwork().getProxyGroups().put(proxyGroup.getName(), proxyGroup);
    }

    public void removePlayerFromWhitelist(final String group, final UUID playerUuid) {
        ProxyGroup proxyGroup = KlarCloudController.getInstance().getInternalCloudNetwork().getProxyGroups().get(group);
        proxyGroup.getProxyConfig().getWhitelist().remove(playerUuid);
        Configuration.loadConfiguration(Paths.get("klarcloud/groups/proxies/" + group + ".json")).addProperty("group", proxyGroup).saveAsConfigurationFile(Paths.get("klarcloud/groups/proxies/" + group + ".json"));
        //KlarCloudController.getInstance().getInternalCloudNetwork().getProxyGroups().remove(group);
        //KlarCloudController.getInstance().getInternalCloudNetwork().getProxyGroups().put(proxyGroup.getName(), proxyGroup);
    }
}
