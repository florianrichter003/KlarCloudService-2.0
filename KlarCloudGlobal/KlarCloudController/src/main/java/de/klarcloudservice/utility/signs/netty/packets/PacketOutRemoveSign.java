/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.utility.signs.netty.packets;

import de.klarcloudservice.configurations.Configuration;
import de.klarcloudservice.netty.packet.Packet;
import de.klarcloudservice.netty.packet.enums.PacketSender;
import de.klarcloudservice.netty.packet.enums.QueryType;
import de.klarcloudservice.signs.KlarCloudSign;

import java.util.Collections;

/**
 * @author _Klaro | Pasqual K. / created on 12.12.2018
 */

public final class PacketOutRemoveSign extends Packet {
    public PacketOutRemoveSign(final KlarCloudSign klarCloudSign) {
        super("RemoveSign", new Configuration().addProperty("klarCloudSign", klarCloudSign), Collections.singletonList(QueryType.COMPLETE), PacketSender.CONTROLLER);
    }
}
