/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.utility.signs.netty.packets;

import de.klarcloudservice.configurations.Configuration;
import de.klarcloudservice.netty.packet.Packet;
import de.klarcloudservice.netty.packet.enums.PacketSender;
import de.klarcloudservice.netty.packet.enums.QueryType;
import de.klarcloudservice.signs.KlarCloudSign;
import de.klarcloudservice.signs.SignLayoutConfiguration;

import java.util.Collections;
import java.util.Map;
import java.util.UUID;

/**
 * @author _Klaro | Pasqual K. / created on 12.12.2018
 */

public final class PacketOutSendSigns extends Packet {
    public PacketOutSendSigns(final SignLayoutConfiguration signLayoutConfiguration, final Map<UUID, KlarCloudSign> signs) {
        super("Signs", new Configuration().addProperty("configuration", signLayoutConfiguration).addProperty("signs", signs), Collections.singletonList(QueryType.COMPLETE), PacketSender.CONTROLLER);
    }
}
