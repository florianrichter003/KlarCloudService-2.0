/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.utility.signs.netty.in;

import com.google.gson.reflect.TypeToken;
import de.klarcloudservice.KlarCloudController;
import de.klarcloudservice.configurations.Configuration;
import de.klarcloudservice.netty.interfaces.NettyAdaptor;
import de.klarcloudservice.netty.packet.enums.QueryType;
import de.klarcloudservice.signs.KlarCloudSign;
import de.klarcloudservice.utility.signs.SignSelector;
import de.klarcloudservice.utility.signs.netty.packets.PacketOutCreateSign;

import java.util.List;

/**
 * @author _Klaro | Pasqual K. / created on 12.12.2018
 */

public class PacketInCreateSign implements NettyAdaptor {
    @Override
    public void handle(Configuration configuration, List<QueryType> queryTypes) {
        SignSelector.getInstance().getSignConfiguration().addSign(configuration.getValue("klarCloudSign", new TypeToken<KlarCloudSign>() {
        }.getType()));
        KlarCloudController.getInstance().getChannelHandler().sendToAllAsynchronous(new PacketOutCreateSign(configuration.getValue("klarCloudSign", new TypeToken<KlarCloudSign>() {
        }.getType())));
    }
}
