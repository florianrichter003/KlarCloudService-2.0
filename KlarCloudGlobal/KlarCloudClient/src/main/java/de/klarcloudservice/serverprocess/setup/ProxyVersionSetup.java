/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.serverprocess.setup;

import de.klarcloudservice.KlarCloudClient;
import de.klarcloudservice.utility.files.DownloadManager;
import jline.console.ConsoleReader;

import java.io.IOException;

/**
 * @author _Klaro | Pasqual K. / created on 30.10.2018
 */

public class ProxyVersionSetup {
    private static final String PATH = "klarcloud/files/BungeeCord.jar";

    /**
     * Starts the Setup of the ProxyVersion
     *
     * @param consoleReader
     */
    public ProxyVersionSetup(final ConsoleReader consoleReader) {
        KlarCloudClient.getInstance().getKlarCloudConsoleLogger().emptyLine().info("Please choose a BungeeCord Version");
        KlarCloudClient.getInstance().getKlarCloudConsoleLogger().info("   \"BungeeCord\",");
        KlarCloudClient.getInstance().getKlarCloudConsoleLogger().info("   \"Waterfall\",");
        KlarCloudClient.getInstance().getKlarCloudConsoleLogger().info("   \"Hexacord\",");
        KlarCloudClient.getInstance().getKlarCloudConsoleLogger().info("   \"Travertine\"");

        String version = "";
        while (version.equals("")) {
            try {
                version = consoleReader.readLine();

                switch (version.toLowerCase()) {
                    case "bungeecord":
                        DownloadManager.download("BungeeCord", "https://ci.md-5.net/job/BungeeCord/lastSuccessfulBuild/artifact/bootstrap/target/BungeeCord.jar", PATH);
                        break;
                    case "waterfall":
                        DownloadManager.download("Waterfall", "https://destroystokyo.com/ci/job/Waterfall/lastSuccessfulBuild/artifact/Waterfall-Proxy/bootstrap/target/Waterfall.jar", PATH);
                        break;
                    case "hexacord":
                        DownloadManager.download("HexaCord", "https://github.com/HexagonMC/BungeeCord/releases/download/v222/BungeeCord.jar", PATH);
                        break;
                    case "travertine":
                        DownloadManager.download("Travertine", "https://papermc.io/ci/job/Travertine/lastSuccessfulBuild/artifact/Travertine-Proxy/bootstrap/target/Travertine.jar", PATH);
                        break;
                    default:
                        KlarCloudClient.getInstance().getKlarCloudConsoleLogger().warn("This Proxy-Version is not supported, yet");
                        version = "";
                        break;
                }
            } catch (final IOException ex) {
                ex.printStackTrace();
            }
        }
    }
}
