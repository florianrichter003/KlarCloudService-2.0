/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.serverprocess.startup;

import de.klarcloudservice.KlarCloudClient;
import de.klarcloudservice.KlarCloudLibrary;
import de.klarcloudservice.configurations.Configuration;
import de.klarcloudservice.meta.CloudProcess;
import de.klarcloudservice.meta.enums.ServerModeType;
import de.klarcloudservice.meta.enums.TemplateBackend;
import de.klarcloudservice.meta.info.ServerInfo;
import de.klarcloudservice.meta.startup.ServerStartupInfo;
import de.klarcloudservice.meta.startup.stages.ProcessStartupStage;
import de.klarcloudservice.netty.packets.PacketOutAddProcess;
import de.klarcloudservice.netty.packets.PacketOutRemoveProcess;
import de.klarcloudservice.netty.packets.PacketOutSendControllerConsoleMessage;
import de.klarcloudservice.netty.packets.PacketOutUpdateInternalCloudNetwork;
import de.klarcloudservice.template.TemplatePreparer;
import de.klarcloudservice.utility.StringUtil;
import de.klarcloudservice.utility.files.FileUtils;
import de.klarcloudservice.utility.files.ZoneInformationProtocolUtility;
import lombok.Getter;

import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Properties;

/**
 * @author _Klaro | Pasqual K. / created on 30.10.2018
 */

@Getter
public class CloudServerStartupHandler {
    private Path path;
    private ServerStartupInfo serverStartupInfo;
    private Process process;
    private int port;

    private ProcessStartupStage processStartupStage;

    /**
     * Creates a instance of a CloudServerStartupHandler
     *
     * @param serverStartupInfo
     */
    public CloudServerStartupHandler(final ServerStartupInfo serverStartupInfo) {
        this.processStartupStage = ProcessStartupStage.WAITING;
        this.serverStartupInfo = serverStartupInfo;
        this.path = Paths.get("klarcloud/temp/servers/" + serverStartupInfo.getName() + "-" + serverStartupInfo.getUid());
    }

    /**
     * Starts the SpigotServer
     *
     * @return {@code true} if the Client could start the SpigotServer
     * or {@code false} if the Client couldn't start the SpigotServer
     */
    public boolean bootstrap() {
        FileUtils.deleteFullDirectory(path);
        FileUtils.createDirectory(path);

        this.processStartupStage = ProcessStartupStage.COPY;
        this.sendMessage("KlarCloud copies custom Template to \"" + path + "\", this may take a long time...");
        if (serverStartupInfo.getServerGroup().getTemplate().getTemplateBackend().equals(TemplateBackend.URL)
                && serverStartupInfo.getServerGroup().getTemplate().getTemplate_url() != null) {
            new TemplatePreparer(path + "/template.zip").loadTemplate(serverStartupInfo.getServerGroup().getTemplate().getTemplate_url());
            ZoneInformationProtocolUtility.extract(path + "/template.zip", path + "");
        } else if (serverStartupInfo.getServerGroup().getTemplate().getTemplateBackend().equals(TemplateBackend.CLIENT)) {
            FileUtils.copyAllFiles(Paths.get("klarcloud/templates/" + serverStartupInfo.getServerGroup().getName()), path + StringUtil.EMPTY);
        } else
            return false;

        if (!Files.exists(Paths.get(path + "/plugins")))
            FileUtils.createDirectory(Paths.get(path + "/plugins"));

        FileUtils.createDirectory(Paths.get(path + "/klarcloud"));

        FileUtils.copyAllFiles(Paths.get("klarcloud/default/servers"), path + StringUtil.EMPTY);

        this.processStartupStage = ProcessStartupStage.PREPARING;
        FileUtils.copyCompiledFile("klarcloud/spigot.yml", path + "/spigot.yml");
        FileUtils.copyCompiledFile("klarcloud/server.properties", path + "/server.properties");

        this.port = KlarCloudClient.getInstance().getInternalCloudNetwork()
                .getServerProcessManager().nextFreePort(serverStartupInfo.getServerGroup().getStartPort());

        Properties properties = new Properties();
        try (InputStreamReader inputStreamReader = new InputStreamReader(Files.newInputStream(Paths.get(path + "/server.properties")))) {
            properties.load(inputStreamReader);
        } catch (final IOException ex) {
            ex.printStackTrace();
            return false;
        }

        properties.setProperty("server-ip", KlarCloudClient.getInstance().getClientConfiguration().getStartIP());
        properties.setProperty("server-port", port + StringUtil.EMPTY);
        properties.setProperty("server-name", serverStartupInfo.getName());
        properties.setProperty("max-players", serverStartupInfo.getServerGroup().getAdvancedConfiguration().getMaxPlayers() + StringUtil.EMPTY);
        properties.setProperty("motd", serverStartupInfo.getServerGroup().getMotd());
        properties.setProperty("allow-nether", this.serverStartupInfo.getServerGroup().getAdvancedConfiguration().isDisableNether() + StringUtil.EMPTY);
        properties.setProperty("spawn-monsters", this.serverStartupInfo.getServerGroup().getAdvancedConfiguration().isSpawnMonsters() + StringUtil.EMPTY);
        properties.setProperty("enable-command-block", this.serverStartupInfo.getServerGroup().getAdvancedConfiguration().isEnableCommandBlock() + StringUtil.EMPTY);
        properties.setProperty("spawn-animals", this.serverStartupInfo.getServerGroup().getAdvancedConfiguration().isSpawnAnimals() + StringUtil.EMPTY);
        properties.setProperty("pvp", this.serverStartupInfo.getServerGroup().getAdvancedConfiguration().isPVP() + StringUtil.EMPTY);
        properties.setProperty("hardcore", this.serverStartupInfo.getServerGroup().getAdvancedConfiguration().isHardcore() + StringUtil.EMPTY);
        properties.setProperty("generate-structures", this.serverStartupInfo.getServerGroup().getAdvancedConfiguration().isGenerateStructures() + StringUtil.EMPTY);
        properties.setProperty("allow-flight", this.serverStartupInfo.getServerGroup().getAdvancedConfiguration().isFly() + StringUtil.EMPTY);
        properties.setProperty("enable-query", this.serverStartupInfo.getServerGroup().getAdvancedConfiguration().isQuery() + StringUtil.EMPTY);
        properties.setProperty("announce-player-achievements", this.serverStartupInfo.getServerGroup().getAdvancedConfiguration().isAnnouncePlayerAchievements() + StringUtil.EMPTY);
        properties.setProperty("force-gamemode", this.serverStartupInfo.getServerGroup().getAdvancedConfiguration().isForceGamemode() + StringUtil.EMPTY);
        properties.setProperty("level-name", this.serverStartupInfo.getServerGroup().getAdvancedConfiguration().getWorldName());
        properties.setProperty("resource-pack", this.serverStartupInfo.getServerGroup().getAdvancedConfiguration().getResourcePack());
        properties.setProperty("view-distance", this.serverStartupInfo.getServerGroup().getAdvancedConfiguration().getViewDistance() + StringUtil.EMPTY);
        properties.setProperty("player-idle-timeout", this.serverStartupInfo.getServerGroup().getAdvancedConfiguration().getIdleTimeout() + StringUtil.EMPTY);
        properties.setProperty("difficulty", this.serverStartupInfo.getServerGroup().getAdvancedConfiguration().getDifficulty() + StringUtil.EMPTY);
        properties.setProperty("gamemode", this.serverStartupInfo.getServerGroup().getAdvancedConfiguration().getDefaultGamemode() + StringUtil.EMPTY);
        properties.setProperty("max-build-height", this.serverStartupInfo.getServerGroup().getAdvancedConfiguration().getMaxBuildHeight() + StringUtil.EMPTY);

        try (OutputStream outputStream = Files.newOutputStream(Paths.get(path + "/server.properties"))) {
            properties.store(outputStream, "");
        } catch (final IOException ex) {
            ex.printStackTrace();
            return false;
        }

        if (!Files.exists(Paths.get(path + "/spigot.jar")))
            FileUtils.copyFile("klarcloud/files/spigot.jar", path + "/spigot.jar");

        FileUtils.deleteFileIfExists(Paths.get(path + "/plugins/KlarCloudAPISpigot.jar"));
        FileUtils.copyCompiledFile("files/KlarCloudAPISpigot.jar", path + "/plugins/KlarCloudAPISpigot.jar");

        ServerInfo serverInfo = new ServerInfo(
                new CloudProcess(serverStartupInfo.getName(), serverStartupInfo.getUid(), KlarCloudClient.getInstance().getClientConfiguration().getClientName(),
                        serverStartupInfo.getId()),
                serverStartupInfo.getServerGroup(), serverStartupInfo.getServerGroup().getName(), KlarCloudClient.getInstance().getClientConfiguration().getStartIP(),
                serverStartupInfo.getServerGroup().getMotd(), this.port, 0, serverStartupInfo.getServerGroup().getMemory(), serverStartupInfo.getServerGroup().isMaintenance(),
                false, new ArrayList<>()
        );

        new Configuration()
                .addProperty("info", serverInfo)
                .addProperty("address", KlarCloudClient.getInstance().getClientConfiguration().getKlarCloudAddresses())
                .addStringProperty("controllerKey", KlarCloudClient.getInstance().getClientConfiguration().getControllerKey())
                .addProperty("startupInfo", serverStartupInfo)

                .saveAsConfigurationFile(Paths.get(path + "/klarcloud/config.json"));

        this.processStartupStage = ProcessStartupStage.START;
        final String[] cmd = new String[]
                {
                        StringUtil.JAVA,
                        "-XX:+UseG1GC",
                        "-XX:MaxGCPauseMillis=50",
                        "-XX:-UseAdaptiveSizePolicy",
                        "-XX:CompileThreshold=100",
                        "-Dfile.encoding=UTF-8",
                        "-Dio.netty.leakDetectionLevel=DISABLED",
                        "-Djline.terminal=jline.UnsupportedTerminal",
                        "-Dcom.mojang.eula.agree=true",
                        "-Xmx" + this.serverStartupInfo.getServerGroup().getMemory() + "M",
                        StringUtil.JAVA_JAR,
                        "spigot.jar",
                        "nogui"
                };

        try {
            this.process = Runtime.getRuntime().exec(cmd, null, new File(path + ""));
        } catch (final IOException ex) {
            ex.printStackTrace();
            return false;
        }

        KlarCloudClient.getInstance().getInternalCloudNetwork().getServerProcessManager().registerServerProcess(
                serverStartupInfo.getUid(), serverStartupInfo.getName(), serverInfo, port
        );
        KlarCloudClient.getInstance().getChannelHandler().sendPacketSynchronized("KlarCloudController", new PacketOutUpdateInternalCloudNetwork(KlarCloudClient.getInstance().getInternalCloudNetwork()));
        KlarCloudClient.getInstance().getChannelHandler().sendPacketSynchronized("KlarCloudController", new PacketOutAddProcess(serverInfo));

        KlarCloudClient.getInstance().getCloudProcessScreenService().registerServerProcess(serverStartupInfo.getName(), this);
        this.processStartupStage = ProcessStartupStage.DONE;
        return true;
    }

    /**
     * Checks if the ServerProcess is alive
     *
     * @return {@code true} if the ServerProcess is alive or {@code false} if the ServerProcess isn't alive
     * @see Process#isAlive()
     * @see Process#getInputStream()
     */
    private boolean isAlive() {
        try {
            return process != null && process.getInputStream().available() != -1 && process.isAlive();
        } catch (final Throwable throwable) {
            return false;
        }
    }

    /**
     * Stops the ServerProcess
     *
     * @return {@code true} if the Client could stop the SpigotProcess or
     * {@code false} if the Client couldn't stop the SpigotProcess
     * @see CloudServerStartupHandler#isAlive()
     */
    public boolean shutdown() {
        this.executeCommand("stop");
        KlarCloudLibrary.sleep(250);

        if (this.isAlive())
            this.process.destroyForcibly();
        KlarCloudLibrary.sleep(250);

        if (this.serverStartupInfo.getServerGroup().getServerModeType().equals(ServerModeType.STATIC)) {
            FileUtils.copyAllFiles(path, "klarcloud/templates/" + serverStartupInfo.getServerGroup().getName(), "spigot.jar");
            KlarCloudLibrary.sleep(250);
        }

        FileUtils.deleteFullDirectory(path);

        KlarCloudClient.getInstance().getChannelHandler().sendPacketSynchronized("KlarCloudController",
                new PacketOutRemoveProcess(KlarCloudClient.getInstance().getInternalCloudNetwork().getServerProcessManager().getRegisteredServerByUID(this.serverStartupInfo.getUid()
                )));

        KlarCloudClient.getInstance().getCloudProcessScreenService().unregisterServerProcess(this.serverStartupInfo.getName());
        KlarCloudClient.getInstance().getInternalCloudNetwork().getServerProcessManager().unregisterServerProcess(
                this.serverStartupInfo.getUid(), this.serverStartupInfo.getName(), this.port
        );
        KlarCloudClient.getInstance().getChannelHandler().sendPacketSynchronized("KlarCloudController", new PacketOutUpdateInternalCloudNetwork(KlarCloudClient.getInstance().getInternalCloudNetwork()));

        try {
            this.finalize();
        } catch (final Throwable ignored) {
        }

        return true;
    }

    /**
     * Executes a command on the SpigotProcess
     *
     * @param command
     * @see Process#getOutputStream()
     * @see OutputStream#write(byte[])
     */
    public void executeCommand(String command) {
        if (!this.isAlive()) return;

        try {
            process.getOutputStream().write((command + "\n").getBytes());
            process.getOutputStream().flush();
        } catch (final IOException ignored) {
        }
    }

    /**
     * Sends a message to KlarCloudController and to Client Console
     *
     * @param message
     * @see PacketOutSendControllerConsoleMessage
     */
    private void sendMessage(final String message) {
        KlarCloudClient.getInstance().getKlarCloudConsoleLogger().info(message);
        KlarCloudClient.getInstance().getChannelHandler().sendPacketSynchronized("KlarCloudController", new PacketOutSendControllerConsoleMessage(message));
    }
}
