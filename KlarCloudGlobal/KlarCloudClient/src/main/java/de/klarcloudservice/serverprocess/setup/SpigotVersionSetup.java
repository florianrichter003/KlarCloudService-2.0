/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.serverprocess.setup;

import de.klarcloudservice.KlarCloudClient;
import de.klarcloudservice.utility.files.DownloadManager;
import jline.console.ConsoleReader;

import java.io.IOException;

/**
 * @author _Klaro | Pasqual K. / created on 30.10.2018
 */

public class SpigotVersionSetup {
    private final ConsoleReader consoleReader;
    private static final String PATH = "klarcloud/files/spigot.jar";

    /**
     * Starts the setup of the MinecraftServerVersion
     *
     * @param consoleReader
     */
    public SpigotVersionSetup(final ConsoleReader consoleReader) {
        this.consoleReader = consoleReader;

        KlarCloudClient.getInstance().getKlarCloudConsoleLogger().emptyLine().info("Please choose a SpigotServer Version");
        KlarCloudClient.getInstance().getKlarCloudConsoleLogger().info("   \"Spigot\" [1.8.8-1.13.2],");
        KlarCloudClient.getInstance().getKlarCloudConsoleLogger().info("   \"Paper\" [1.8.8-1.13.2],");
        KlarCloudClient.getInstance().getKlarCloudConsoleLogger().info("   \"Taco\" [1.8.8-1.12.2],");
        KlarCloudClient.getInstance().getKlarCloudConsoleLogger().info("   \"ShortSpigot\" [1.12.2]");

        String version = "";
        while (version.equals("")) {
            try {
                version = this.consoleReader.readLine();

                switch (version.toLowerCase()) {
                    case "spigot":
                        this.setupSpigotVersion();
                        break;
                    case "paper":
                        this.setupPaperVersion();
                        break;
                    case "taco":
                        this.setupTacoVersion();
                        break;
                    case "shortspigot":
                        DownloadManager.downloadAndDisconnect("ShortSpigot[/1.12.2]", "https://dl.shortspigot.sh/file/latest", PATH);
                        break;
                    default:
                        KlarCloudClient.getInstance().getKlarCloudConsoleLogger().warn("This SpigotServer-Version is not supported, yet");
                        version = "";
                        break;
                }
            } catch (final IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    /**
     * Setup of SpigotVersions
     */
    private void setupSpigotVersion() {
        KlarCloudClient.getInstance().getKlarCloudConsoleLogger().emptyLine().info("Please choose a MinecraftServer Version");
        KlarCloudClient.getInstance().getKlarCloudConsoleLogger().info("   \"1.8.8\",");
        KlarCloudClient.getInstance().getKlarCloudConsoleLogger().info("   \"1.10.2\",");
        KlarCloudClient.getInstance().getKlarCloudConsoleLogger().info("   \"1.11.2\",");
        KlarCloudClient.getInstance().getKlarCloudConsoleLogger().info("   \"1.12.2\"");
        KlarCloudClient.getInstance().getKlarCloudConsoleLogger().info("   \"1.13.2\"");

        String version = "";
        while (version.equals("")) {
            try {
                version = this.consoleReader.readLine();

                switch (version.toLowerCase()) {
                    case "1.8.8":
                        DownloadManager.downloadAndDisconnect("Spigot[/1.8.8]", "https://archive.mcmirror.io/Spigot/spigot-1.8.8-R0.1-SNAPSHOT-latest.jar", PATH);
                        break;
                    case "1.10.2":
                        DownloadManager.downloadAndDisconnect("Spigot[/1.10.2]", "https://archive.mcmirror.io/Spigot/spigot-1.10.2-R0.1-SNAPSHOT-latest.jar", PATH);
                        break;
                    case "1.11.2":
                        DownloadManager.downloadAndDisconnect("Spigot[/1.11.2]", "https://archive.mcmirror.io/Spigot/spigot-api-1.11.2-R0.1-SNAPSHOT.jar", PATH);
                        break;
                    case "1.12.2":
                        DownloadManager.downloadAndDisconnect("Spigot[/1.12.2]", "https://mcmirror.io/files/Spigot/Spigot-1.12.2-e8ded36-20181110-0947.jar", PATH);
                        break;
                    case "1.13.2":
                        DownloadManager.downloadAndDisconnect("Spigot[/1.13.2]", "https://mcmirror.io/files/Spigot/Spigot-1.13.2-f56e2e7-20190106-2330.jar", PATH);
                        break;
                    default:
                        KlarCloudClient.getInstance().getKlarCloudConsoleLogger().warn("This MinecraftServer-Version is not supported, yet");
                        version = "";
                        break;
                }
            } catch (final IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    /**
     * Setup of PaperVersions
     */
    private void setupPaperVersion() {
        KlarCloudClient.getInstance().getKlarCloudConsoleLogger().emptyLine().info("Please choose a MinecraftServer Version");
        KlarCloudClient.getInstance().getKlarCloudConsoleLogger().info("   \"1.8.8\",");
        KlarCloudClient.getInstance().getKlarCloudConsoleLogger().info("   \"1.11.2\",");
        KlarCloudClient.getInstance().getKlarCloudConsoleLogger().info("   \"1.12.2\"");
        KlarCloudClient.getInstance().getKlarCloudConsoleLogger().info("   \"1.13.2\"");

        String version = "";
        while (version.equals("")) {
            try {
                version = this.consoleReader.readLine();

                switch (version.toLowerCase()) {
                    case "1.8.8":
                        DownloadManager.downloadAndDisconnect("Paper[/1.8.8]", "https://archive.mcmirror.io/Paper/Paper-1.8.8-R0.1-SNAPSHOT-latest.jar", PATH);
                        break;
                    case "1.11.2":
                        DownloadManager.downloadAndDisconnect("Paper[/1.11.2]", "https://archive.mcmirror.io/Paper/Paper-1.11.2-b1000.jar", PATH);
                        break;
                    case "1.12.2":
                        DownloadManager.downloadAndDisconnect("Paper[/1.12.2]", "https://mcmirror.io/files/Paper/Paper-1.12.2-ac69748-20181207-0309.jar", PATH);
                        break;
                    case "1.13.2":
                        DownloadManager.downloadAndDisconnect("Paper[/1.13.2]", "https://mcmirror.io/files/Paper/Paper-1.13.2-44e66f7-20190121-0708.jar", PATH);
                        break;
                    default:
                        KlarCloudClient.getInstance().getKlarCloudConsoleLogger().warn("This MinecraftServer-Version is not supported, yet");
                        version = "";
                        break;
                }
            } catch (final IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    /**
     * Setup of TacoVersions
     */
    private void setupTacoVersion() {
        KlarCloudClient.getInstance().getKlarCloudConsoleLogger().emptyLine().info("Please choose a MinecraftServer Version");
        KlarCloudClient.getInstance().getKlarCloudConsoleLogger().info("   \"1.8.8\",");
        KlarCloudClient.getInstance().getKlarCloudConsoleLogger().info("   \"1.11.2\",");
        KlarCloudClient.getInstance().getKlarCloudConsoleLogger().info("   \"1.12.2\"");

        String version = "";
        while (version.equals("")) {
            try {
                version = this.consoleReader.readLine();

                switch (version.toLowerCase()) {
                    case "1.8.8":
                        DownloadManager.downloadAndDisconnect("Taco[/1.8.8]", "https://archive.mcmirror.io/Taco%20Spigot/TacoSpigot-1.8.8-latest.jar", PATH);
                        break;
                    case "1.11":
                        DownloadManager.downloadAndDisconnect("Taco[/1.11]", "https://archive.mcmirror.io/Taco%20Spigot/TacoSpigot-1.11-latest.jar", PATH);
                        break;
                    case "1.11.2":
                        DownloadManager.downloadAndDisconnect("Taco[/1.11.2]", "https://archive.mcmirror.io/Taco%20Spigot/TacoSpigot-1.11.2-b102.jar", PATH);
                        break;
                    case "1.12":
                        DownloadManager.downloadAndDisconnect("Taco[/1.12]", "https://archive.mcmirror.io/Taco%20Spigot/TacoSpigot-1.12-b105.jar", PATH);
                        break;
                    case "1.12.2":
                        DownloadManager.downloadAndDisconnect("Taco[/1.12.2]", "https://archive.mcmirror.io/Taco%20Spigot/TacoSpigot-1.12.2-b114.jar", PATH);
                        break;
                    default:
                        KlarCloudClient.getInstance().getKlarCloudConsoleLogger().warn("This MinecraftServer-Version is not supported, yet");
                        version = "";
                        break;
                }
            } catch (final IOException ex) {
                ex.printStackTrace();
            }
        }
    }
}
