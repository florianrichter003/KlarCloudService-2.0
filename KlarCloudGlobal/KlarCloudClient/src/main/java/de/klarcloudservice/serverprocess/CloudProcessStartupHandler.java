/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.serverprocess;

import de.klarcloudservice.KlarCloudClient;
import de.klarcloudservice.KlarCloudLibrary;
import de.klarcloudservice.meta.startup.ProxyStartupInfo;
import de.klarcloudservice.meta.startup.ServerStartupInfo;
import de.klarcloudservice.netty.packets.PacketOutSendControllerConsoleMessage;
import de.klarcloudservice.serverprocess.startup.CloudServerStartupHandler;
import de.klarcloudservice.serverprocess.startup.ProxyStartupHandler;
import de.klarcloudservice.utility.files.FileUtils;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedDeque;

/**
 * @author _Klaro | Pasqual K. / created on 29.10.2018
 */

public class CloudProcessStartupHandler implements Runnable {
    private final Queue<ServerStartupInfo> serverStartupInfo = new ConcurrentLinkedDeque<>();
    private final Queue<ProxyStartupInfo> proxyStartupInfo = new ConcurrentLinkedDeque<>();

    /**
     * Offers a serverProcess
     *
     * @param serverStartupInfo
     */
    public void offerServerProcess(ServerStartupInfo serverStartupInfo) {
        this.serverStartupInfo.add(serverStartupInfo);
    }

    /**
     * Offers a proxyProcess
     *
     * @param proxyStartupInfo
     */
    public void offerProxyProcess(ProxyStartupInfo proxyStartupInfo) {
        this.proxyStartupInfo.add(proxyStartupInfo);
    }

    @Override
    public void run() {
        while (!Thread.currentThread().isInterrupted()) {
            while (KlarCloudClient.RUNNING) {
                KlarCloudLibrary.sleep(Thread.currentThread(), 750L);

                while (!this.serverStartupInfo.isEmpty()) {
                    final ServerStartupInfo serverStartupInfo = this.serverStartupInfo.poll();

                    if (!Files.exists(Paths.get("klarcloud/templates/" + serverStartupInfo.getServerGroup().getName())))
                        FileUtils.createDirectory(Paths.get("klarcloud/templates/" + serverStartupInfo.getServerGroup().getName() + "/plugins"));

                    if (!KlarCloudClient.getInstance().getInternalCloudNetwork().getServerProcessManager().isNameServerProcessRegistered(serverStartupInfo.getName())
                            && (KlarCloudClient.getInstance().getMemory() + serverStartupInfo.getServerGroup().getMemory()) <
                            KlarCloudClient.getInstance().getClientConfiguration().getMemory() && (KlarCloudClient.getInstance().getClientConfiguration().getCpu() == 0D || KlarCloudLibrary.cpuUsage() <= KlarCloudClient.getInstance().getClientConfiguration().getCpu())) {
                        this.send("Waiting for ServerStartup [Name=" + serverStartupInfo.getName()
                                + "/UID=" + serverStartupInfo.getUid() + "/Group=" + serverStartupInfo.getServerGroup().getName() + "/Service=CloudServer] to collect all " +
                                "needed information...");

                        if (!new CloudServerStartupHandler(serverStartupInfo).bootstrap())
                            this.serverStartupInfo.add(serverStartupInfo);
                        else {
                            this.send("ServerStartup [Name=" + serverStartupInfo.getName()
                                    + "/UID=" + serverStartupInfo.getUid() + "/Group=" + serverStartupInfo.getServerGroup().getName() + "/Service=CloudServer] has" +
                                    " collected all information");
                            this.send("Please wait while the Process is starting up...");
                        }
                    } else
                        this.serverStartupInfo.add(serverStartupInfo);
                }

                while (!this.proxyStartupInfo.isEmpty()) {
                    final ProxyStartupInfo proxyStartupInfo = this.proxyStartupInfo.poll();

                    if (!Files.exists(Paths.get("klarcloud/templates/" + proxyStartupInfo.getProxyGroup().getName())))
                        FileUtils.createDirectory(Paths.get("klarcloud/templates/" + proxyStartupInfo.getProxyGroup().getName() + "/plugins"));

                    if (!KlarCloudClient.getInstance().getInternalCloudNetwork().getServerProcessManager().isNameProxyProcessRegistered(proxyStartupInfo.getName())
                            && (KlarCloudClient.getInstance().getMemory() + proxyStartupInfo.getProxyGroup().getMemory()) <
                            KlarCloudClient.getInstance().getClientConfiguration().getMemory() && (KlarCloudClient.getInstance().getClientConfiguration().getCpu() == 0D || KlarCloudLibrary.cpuUsage() <= KlarCloudClient.getInstance().getClientConfiguration().getCpu())) {
                        this.send("Waiting for ProxyStartup [Name=" + proxyStartupInfo.getName()
                                + "/UID=" + proxyStartupInfo.getUid() + "/Group=" + proxyStartupInfo.getProxyGroup().getName() + "/Service=Proxy] to collect all " +
                                "needed information...");

                        if (!new ProxyStartupHandler(proxyStartupInfo).bootstrap())
                            this.proxyStartupInfo.add(proxyStartupInfo);
                        else {
                            this.send("ProxyStartup [Name=" + proxyStartupInfo.getName()
                                    + "/UID=" + proxyStartupInfo.getUid() + "/Group=" + proxyStartupInfo.getProxyGroup().getName() + "/Service=Proxy] has" +
                                    " collected all information");
                            this.send("Please wait while the Process is starting up...");
                        }
                    } else {
                        this.proxyStartupInfo.add(proxyStartupInfo);
                    }
                }
            }
        }
    }

    /**
     * Sends a message to KlarCloudController and to Client Console
     *
     * @param message
     * @see PacketOutSendControllerConsoleMessage
     */
    private void send(String message) {
        KlarCloudClient.getInstance().getKlarCloudConsoleLogger().info(message);
        KlarCloudClient.getInstance().getChannelHandler().sendPacketSynchronized("KlarCloudController", new PacketOutSendControllerConsoleMessage(message));
    }
}
