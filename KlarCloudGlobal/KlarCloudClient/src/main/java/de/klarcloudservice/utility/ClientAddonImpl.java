/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.utility;

import de.klarcloudservice.KlarCloudClient;
import de.klarcloudservice.KlarCloudLibraryService;
import de.klarcloudservice.addons.JavaAddon;
import de.klarcloudservice.commands.interfaces.Command;
import de.klarcloudservice.event.Listener;

import java.util.Arrays;

/**
 * @author _Klaro | Pasqual K. / created on 27.12.2018
 */

public class ClientAddonImpl extends JavaAddon<KlarCloudClient> {
    @Override
    public KlarCloudClient getInternalKlarCloudSystem() {
        return KlarCloudClient.getInstance();
    }

    @Override
    public KlarCloudLibraryService getInternalKlarCloudLibrary() {
        return KlarCloudLibraryService.getInstance();
    }

    public void registerCommand(final String name, final Command command) {
        KlarCloudClient.getInstance().getCommandManager().registerCommand(name, command);
    }

    public void registerCommand(final String name, final Command command, final String... aliases) {
        KlarCloudClient.getInstance().getCommandManager().registerCommand(name, command, aliases);
    }

    public void registerListener(final Listener listener) {
        KlarCloudClient.getInstance().getEventManager().registerListener(listener);
    }

    public void registerListener(final Listener... listeners) {
        Arrays.stream(listeners).forEach(e -> KlarCloudClient.getInstance().getEventManager().registerListener(e));
    }
}
