/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice;

import de.klarcloudservice.addons.AddonParallelLoader;
import de.klarcloudservice.commands.CommandExit;
import de.klarcloudservice.commands.CommandHelp;
import de.klarcloudservice.commands.CommandManager;
import de.klarcloudservice.commands.CommandUpdate;
import de.klarcloudservice.configuration.ClientConfiguration;
import de.klarcloudservice.event.EventManager;
import de.klarcloudservice.event.enums.EventTargetType;
import de.klarcloudservice.event.events.LoadSuccessEvent;
import de.klarcloudservice.exceptions.LoadException;
import de.klarcloudservice.logging.KlarCloudConsoleLogger;
import de.klarcloudservice.netty.NettyHandler;
import de.klarcloudservice.netty.NettySocketClient;
import de.klarcloudservice.netty.channel.ChannelHandler;
import de.klarcloudservice.netty.in.*;
import de.klarcloudservice.serverprocess.CloudProcessStartupHandler;
import de.klarcloudservice.serverprocess.screen.CloudProcessScreenService;
import de.klarcloudservice.serverprocess.setup.ProxyVersionSetup;
import de.klarcloudservice.serverprocess.setup.SpigotVersionSetup;
import de.klarcloudservice.utility.StringUtil;
import de.klarcloudservice.utility.cloudsystem.InternalCloudNetwork;
import de.klarcloudservice.utility.cloudsystem.KlarCloudAddresses;
import de.klarcloudservice.utility.cloudsystem.ServerProcessManager;
import de.klarcloudservice.utility.runtime.Reload;
import de.klarcloudservice.utility.runtime.Shutdown;
import de.klarcloudservice.utility.threading.scheduler.Scheduler;
import de.klarcloudservice.versioneering.VersionController;
import lombok.Getter;
import lombok.Setter;

import javax.management.InstanceAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * @author _Klaro | Pasqual K. / created on 23.10.2018
 */

@Getter
@Setter
public class KlarCloudClient implements Shutdown, Reload {
    public static volatile boolean RUNNING = false;

    @Getter
    private static KlarCloudClient instance;

    private KlarCloudConsoleLogger klarCloudConsoleLogger;
    private CommandManager commandManager;

    private boolean ssl;

    private final EventManager eventManager = new EventManager();
    private final AddonParallelLoader addonParallelLoader = new AddonParallelLoader();

    private InternalCloudNetwork internalCloudNetwork = new InternalCloudNetwork();

    private final NettyHandler nettyHandler = new NettyHandler();
    private final ChannelHandler channelHandler = new ChannelHandler();
    private final NettySocketClient nettySocketClient = new NettySocketClient();
    private final Scheduler scheduler = new Scheduler(40);
    private final CloudProcessStartupHandler cloudProcessStartupHandler = new CloudProcessStartupHandler();
    private final CloudProcessScreenService cloudProcessScreenService = new CloudProcessScreenService();

    private ClientConfiguration clientConfiguration;

    /**
     * Creates a new Instance of the {KlarCloudClient}
     *
     * @param klarCloudConsoleLogger
     * @param commandManager
     * @param ssl
     * @param time
     * @throws Throwable
     */
    public KlarCloudClient(KlarCloudConsoleLogger klarCloudConsoleLogger, CommandManager commandManager, boolean ssl, final long time) throws Throwable {
        klarCloudConsoleLogger.info("Trying to create new Client instance...");

        if (instance == null)
            instance = this;
        else
            StringUtil.printError(klarCloudConsoleLogger, "KlarCloudClient Instance already exists", new LoadException(new InstanceAlreadyExistsException()));

        this.ssl = ssl;
        this.commandManager = commandManager;
        this.klarCloudConsoleLogger = klarCloudConsoleLogger;

        this.clientConfiguration = new ClientConfiguration(klarCloudConsoleLogger.getConsoleReader());
        new KlarCloudLibraryService(klarCloudConsoleLogger, this.clientConfiguration.getControllerKey(), clientConfiguration.getKlarCloudAddresses().getHost(), eventManager);

        if (!Files.exists(Paths.get("klarcloud/files/BungeeCord.jar"))) {
            klarCloudConsoleLogger.warn("No \"BungeeCord.jar\" could be found in \"klarcloud/files/\", please make the Setup");
            new ProxyVersionSetup(klarCloudConsoleLogger.getConsoleReader());
        }

        if (!Files.exists(Paths.get("klarcloud/files/spigot.jar"))) {
            klarCloudConsoleLogger.warn("No \"spigot.jar\" could be found in \"klarcloud/files/\", please make the Setup");
            new SpigotVersionSetup(klarCloudConsoleLogger.getConsoleReader());
        }

        this.addonParallelLoader.loadAddons();

        klarCloudConsoleLogger.info("Trying to register Packet-Handlers...");
        nettyHandler.registerHandler("InitializeCloudNetwork", new PacketInInitializeInternal());
        nettyHandler.registerHandler("StartCloudServer", new PacketInStartGameServer());
        nettyHandler.registerHandler("StartProxy", new PacketInStartProxy());
        nettyHandler.registerHandler("UpdateAll", new PacketInUpdateAll());
        nettyHandler.registerHandler("ExecuteCommand", new PacketInExecuteCommand());
        nettyHandler.registerHandler("StopProcess", new PacketInStopProcess());
        nettyHandler.registerHandler("ServerInfoUpdate", new PacketInServerInfoUpdate());
        nettyHandler.registerHandler("CopyServerIntoTemplate", new PacketInCopyServerIntoTemplate());

        klarCloudConsoleLogger.info("The following Handlers are registered:");
        nettyHandler.getHandlers().forEach((handler -> klarCloudConsoleLogger.info("    " + handler)));
        klarCloudConsoleLogger.emptyLine().info("All Handlers are now registered");

        klarCloudConsoleLogger.info("Trying to register CloudCommands...");
        commandManager
                .registerCommand("exit", new CommandExit())
                .registerCommand("update", new CommandUpdate())
                .registerCommand("help", new CommandHelp());

        klarCloudConsoleLogger.info("The following Commands are registered:");
        commandManager.getCommands().forEach((command -> klarCloudConsoleLogger.info("   \"" + command + "\"")));
        klarCloudConsoleLogger.emptyLine().info("All Commands are now registered");

        klarCloudConsoleLogger.info("Trying to startup scheduler thread...");

        Thread thread = new Thread(this.scheduler);
        thread.setPriority(Thread.MIN_PRIORITY);
        thread.setDaemon(true);
        thread.start();

        klarCloudConsoleLogger.info("Scheduler thread is now alive");
        klarCloudConsoleLogger.info("Trying to startup Queue Thread...");

        Thread startup = new Thread(this.cloudProcessStartupHandler, "Startup Handler Thread");
        startup.setPriority(Thread.MIN_PRIORITY);
        startup.setDaemon(true);
        startup.start();

        klarCloudConsoleLogger.info("Queue Thread is now alive");
        klarCloudConsoleLogger.info("Trying to add Runtime-Shutdown-Hook...");

        Runtime.getRuntime().addShutdownHook(new Thread(this::shutdownAll, "ShutdownHook"));

        klarCloudConsoleLogger.info("Shutdown-Hook has been added to Runtime");

        this.connect(ssl);

        this.addonParallelLoader.enableAddons();
        this.checkForUpdates();

        RUNNING = true;

        klarCloudConsoleLogger.info("KlarCloudService has been successfully loaded, took " + (System.currentTimeMillis() - time) + "ms");
        this.eventManager.callEvent(EventTargetType.LOAD_SUCCESS, new LoadSuccessEvent(true));
    }

    @Override
    public void reloadAll() {
    }

    @Override
    public void shutdownAll() {
        RUNNING = false;

        this.nettySocketClient.close();

        this.cloudProcessScreenService.getRegisteredProxyProcesses().forEach(proxyProcess -> {
            this.getKlarCloudConsoleLogger().info("ProxyProcess " + proxyProcess.getProxyStartupInfo().getName() + " stops...");
            proxyProcess.shutdown(null);
            this.getKlarCloudConsoleLogger().info("ProxyProcess " + proxyProcess.getProxyStartupInfo().getName() + " was stopped");
            KlarCloudLibrary.sleep(1000);
        });
        this.cloudProcessScreenService.getRegisteredServerProcesses().forEach(serverProcess -> {
            this.getKlarCloudConsoleLogger().info("ServerProcess " + serverProcess.getServerStartupInfo().getName() + " stops...");
            serverProcess.shutdown();
            this.getKlarCloudConsoleLogger().info("ServerProcess " + serverProcess.getServerStartupInfo().getName() + " was stopped");
            KlarCloudLibrary.sleep(1000);
        });

        this.addonParallelLoader.disableAddons();

        klarCloudConsoleLogger.info("Waiting for background task to finish...");
        KlarCloudLibrary.sleep(3000);

        KlarCloudLibrary.end();
    }

    /**
     * Get used memory of KlarCloudClient
     *
     * @return {@link Integer} of used memory by adding the maximal memory of the ProxyProcesses
     * to the maximal memory of the ServerProcesses
     * @see ServerProcessManager#getUsedProxyMemory()
     * @see ServerProcessManager#getUsedServerMemory()
     */
    public int getMemory() {
        return (this.internalCloudNetwork.getServerProcessManager().getUsedProxyMemory()
                + this.internalCloudNetwork.getServerProcessManager().getUsedServerMemory());
    }

    /**
     * Connects to the KlarCloudController by using {@link Boolean} ssl if the {@link joptsimple.OptionSet} has ssl
     *
     * @param ssl
     * @see NettySocketClient#connect(KlarCloudAddresses, NettyHandler, ChannelHandler, boolean)
     * @see joptsimple.OptionSet#has(String)
     */
    private void connect(final boolean ssl) {
        while (this.nettySocketClient.getConnections() != -1) {
            if (this.nettySocketClient.getConnections() == 8)
                System.exit(1);

            this.nettySocketClient.connect(clientConfiguration.getKlarCloudAddresses(), this.nettyHandler, this.channelHandler, ssl);

            KlarCloudLibrary.sleep(2000);
        }
    }

    public void checkForUpdates() {
        if (VersionController.isVersionAvailable()) {
            klarCloudConsoleLogger.warn("A newer version of KlarCloud is available");
            klarCloudConsoleLogger.warn("Type \"update\" to get more information");
        } else {
            klarCloudConsoleLogger.info("Your version is up-to-date");
        }
    }
}
