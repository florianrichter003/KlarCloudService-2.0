/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.configuration;

import de.klarcloudservice.KlarCloudClient;
import de.klarcloudservice.KlarCloudLibrary;
import de.klarcloudservice.utility.StringUtil;
import de.klarcloudservice.utility.cloudsystem.KlarCloudAddresses;
import de.klarcloudservice.utility.files.FileUtils;
import jline.console.ConsoleReader;
import lombok.Getter;

import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Properties;

/**
 * @author _Klaro | Pasqual K. / created on 24.10.2018
 */

@Getter
public class ClientConfiguration {
    private String controllerKey, controllerIP, clientName, startIP;
    private int memory, controllerPort, controllerWebPort;
    private double cpu;

    private KlarCloudAddresses klarCloudAddresses;

    /**
     * Creates or/and loads the Client Configuration
     *
     * @param consoleReader
     * @throws Throwable
     */
    public ClientConfiguration(final ConsoleReader consoleReader) throws Throwable {
        for (File dir : new File[]{
                new File("klarcloud/templates"),
                new File("klarcloud/default/proxies/plugins"),
                new File("klarcloud/default/servers/plugins"),
                new File("klarcloud/temp/servers"),
                new File("klarcloud/temp/proxies"),
                new File("klarcloud/files")
        })
            dir.mkdirs();

        this.clearProxyTemp();
        this.clearServerTemp();

        KlarCloudClient.getInstance().getKlarCloudConsoleLogger().info("Trying to load KlarCloud-Client Configuration...");

        if (this.defaultInit(consoleReader))
            KlarCloudClient.getInstance().getKlarCloudConsoleLogger().info("Default KlarCloud-Client Configuration has been successfully created");

        this.load();
    }

    /**
     * Creates default Configuration or
     * returns if the Configuration already exists
     *
     * @param consoleReader
     * @return
     * @throws Throwable
     */
    private boolean defaultInit(final ConsoleReader consoleReader) throws Throwable {
        if (Files.exists(Paths.get("configuration.properties"))) return false;

        final String ip = KlarCloudLibrary.getInternalAddress();

        KlarCloudClient.getInstance().getKlarCloudConsoleLogger().info("Please provide the name of this Client. (Recommended: Client-01)");
        String clientID = consoleReader.readLine().replace(" ", "");

        if (clientID.isEmpty())
            clientID = "Client-" + KlarCloudLibrary.THREAD_LOCAL_RANDOM.nextLong();

        Properties properties = new Properties();

        properties.setProperty("controller.ip", ip);
        properties.setProperty("controller.port", 5000 + StringUtil.EMPTY);
        properties.setProperty("controller.web-port", 4790 + StringUtil.EMPTY);
        String line;
        KlarCloudClient.getInstance().getKlarCloudConsoleLogger().info("Please enter the ControllerKey");
        while ((line = consoleReader.readLine()) != null) {
            properties.setProperty("controller.key", line);
            break;
        }
        properties.setProperty("general.client-name", clientID);
        properties.setProperty("general.start-host", ip);
        properties.setProperty("general.memory", 1024 + StringUtil.EMPTY);
        properties.setProperty("general.maxcpuusage", 90.00 + StringUtil.EMPTY);


        try (OutputStream outputStream = Files.newOutputStream(Paths.get("configuration.properties"))) {
            properties.store(outputStream, "KlarCloud default Configuration");
        } catch (final IOException ex) {
            ex.printStackTrace();
            return false;
        }
        return true;
    }

    public void clearServerTemp() {
        final File dir = new File("klarcloud/temp/servers");

        if (dir.isDirectory()) {
            for (File file : dir.listFiles()) {
                if (file.isDirectory()) {
                    FileUtils.deleteFullDirectory(file.toPath());
                }
            }
        }
    }

    public void clearProxyTemp() {
        final File dir = new File("klarcloud/temp/proxies");

        if (dir.isDirectory()) {
            for (File file : dir.listFiles()) {
                if (file.isDirectory()) {
                    FileUtils.deleteFullDirectory(file.toPath());
                }
            }
        }
    }

    /**
     * Loads the ClientConfiguration
     */
    private void load() {
        Properties properties = new Properties();
        try (InputStreamReader inputStreamReader = new InputStreamReader(Files.newInputStream(Paths.get("configuration.properties")))) {
            properties.load(inputStreamReader);
        } catch (final IOException ex) {
            ex.printStackTrace();
        }

        this.controllerKey = properties.getProperty("controller.key");

        this.clientName = properties.getProperty("general.client-name");
        this.startIP = properties.getProperty("general.start-host");
        this.memory = Integer.parseInt(properties.getProperty("general.memory"));

        this.controllerIP = properties.getProperty("controller.ip");
        this.controllerPort = Integer.parseInt(properties.getProperty("controller.port"));
        this.controllerWebPort = Integer.parseInt(properties.getProperty("controller.web-port"));
        this.cpu = Double.parseDouble(properties.getProperty("general.maxcpuusage"));

        this.klarCloudAddresses = new KlarCloudAddresses(this.controllerIP, this.controllerPort);
    }
}
