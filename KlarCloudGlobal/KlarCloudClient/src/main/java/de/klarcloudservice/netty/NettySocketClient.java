/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.netty;

import de.klarcloudservice.KlarCloudClient;
import de.klarcloudservice.KlarCloudLibrary;
import de.klarcloudservice.configurations.Configuration;
import de.klarcloudservice.netty.authentication.enums.AuthenticationType;
import de.klarcloudservice.netty.channel.ChannelHandler;
import de.klarcloudservice.netty.packet.Packet;
import de.klarcloudservice.netty.packet.enums.PacketSender;
import de.klarcloudservice.netty.packet.enums.QueryType;
import de.klarcloudservice.utility.cloudsystem.KlarCloudAddresses;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.handler.ssl.SslContext;
import io.netty.handler.ssl.SslContextBuilder;
import io.netty.handler.ssl.util.InsecureTrustManagerFactory;
import lombok.Getter;

import java.util.Arrays;

/**
 * @author _Klaro | Pasqual K. / created on 24.10.2018
 */

@Getter
public class NettySocketClient implements AutoCloseable {
    private int connections = 1;
    private SslContext sslContext;
    private final EventLoopGroup eventLoopGroup = KlarCloudLibrary.eventLoopGroup(4);

    /**
     * Connects to the KlarCloudController
     *
     * @param klarCloudAddresses
     * @param nettyHandler
     * @param channelHandler
     * @param ssl
     * @see io.netty.bootstrap.ServerBootstrap
     */
    public void connect(KlarCloudAddresses klarCloudAddresses, NettyHandler nettyHandler, ChannelHandler channelHandler, boolean ssl) {
        try {
            KlarCloudClient.getInstance().getKlarCloudConsoleLogger().info("Trying to connect to KlarCloudController @" +
                    klarCloudAddresses.getHost() + ":" + klarCloudAddresses.getPort() + " [" + connections + "/7]");

            if (ssl)
                sslContext = SslContextBuilder.forClient().trustManager(InsecureTrustManagerFactory.INSTANCE).build();

            Bootstrap bootstrap = new Bootstrap()
                    .group(eventLoopGroup)

                    .option(ChannelOption.AUTO_READ, true)
                    .option(ChannelOption.SO_KEEPALIVE, true)
                    .option(ChannelOption.IP_TOS, 24)
                    .option(ChannelOption.TCP_NODELAY, true)

                    .channel(KlarCloudLibrary.klarCloudClientSocketChannel())

                    .handler(new ChannelInitializer<Channel>() {
                        @Override
                        protected void initChannel(Channel channel) {
                            if (ssl && sslContext != null) {
                                channel.pipeline().addLast(sslContext.newHandler(channel.alloc(),
                                        klarCloudAddresses.getHost(), klarCloudAddresses.getPort()));
                            }

                            KlarCloudLibrary.prepareChannel(channel, nettyHandler, channelHandler);
                        }
                    });

            KlarCloudClient.getInstance().getKlarCloudConsoleLogger().info("Using " + (KlarCloudLibrary.EPOLL ? "Epoll transport type." : "Nio transport type"));

            bootstrap.connect(klarCloudAddresses.getHost(), klarCloudAddresses.getPort()).sync().channel().writeAndFlush(new Packet("Auth",
                    new Configuration()
                            .addStringProperty("key", KlarCloudClient.getInstance().getClientConfiguration().getControllerKey())
                            .addStringProperty("name", KlarCloudClient.getInstance().getClientConfiguration().getClientName())
                            .addProperty("AuthenticationType", AuthenticationType.INTERNAL),
                    Arrays.asList(QueryType.COMPLETE, QueryType.NO_RESULT), PacketSender.CLIENT));

            KlarCloudClient.getInstance().getKlarCloudConsoleLogger()
                    .info("KlarCloudService is now ready and connected to " + klarCloudAddresses.getHost() + ":" + klarCloudAddresses.getPort());

            connections = -1;
        } catch (final Throwable throwable) {
            connections++;
            KlarCloudClient.getInstance().getKlarCloudConsoleLogger().err("KlarCloudService could not connect to " + klarCloudAddresses.getHost() + ":" + klarCloudAddresses.getPort());
            KlarCloudClient.getInstance().getKlarCloudConsoleLogger().err("The following error occurred: " + throwable.getCause().toString());
        }
    }

    @Override
    public void close() {
        eventLoopGroup.shutdownGracefully();
    }
}
