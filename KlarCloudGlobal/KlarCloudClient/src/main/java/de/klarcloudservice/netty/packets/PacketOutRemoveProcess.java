/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.netty.packets;

import de.klarcloudservice.configurations.Configuration;
import de.klarcloudservice.meta.info.ProxyInfo;
import de.klarcloudservice.meta.info.ServerInfo;
import de.klarcloudservice.netty.packet.Packet;
import de.klarcloudservice.netty.packet.enums.PacketSender;
import de.klarcloudservice.netty.packet.enums.QueryType;

import java.util.Collections;

/**
 * @author _Klaro | Pasqual K. / created on 11.11.2018
 */

public final class PacketOutRemoveProcess extends Packet {
    public PacketOutRemoveProcess(final ServerInfo serverInfo) {
        super("ProcessRemove", new Configuration().addProperty("serverInfo", serverInfo), Collections.singletonList(QueryType.COMPLETE), PacketSender.CLIENT);
    }

    public PacketOutRemoveProcess(final ProxyInfo proxyInfo) {
        super("ProcessRemove", new Configuration().addProperty("proxyInfo", proxyInfo), Collections.singletonList(QueryType.COMPLETE), PacketSender.CLIENT);
    }
}
