/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.bootstrap;

import de.klarcloudservice.KlarCloudClient;
import de.klarcloudservice.KlarCloudLibrary;
import de.klarcloudservice.commands.CommandManager;
import de.klarcloudservice.logging.KlarCloudConsoleLogger;
import de.klarcloudservice.utility.StringUtil;
import de.klarcloudservice.utility.files.FileUtils;
import de.klarcloudservice.utility.time.DateProvider;
import io.netty.util.ResourceLeakDetector;
import joptsimple.OptionParser;
import joptsimple.OptionSet;

import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * @author _Klaro | Pasqual K. / created on 23.10.2018
 */

final class KlarCloudClientBootstrap {
    /**
     * Main Method of KlarCloudClient
     *
     * @param args
     * @throws Throwable
     */
    public static synchronized void main(String[] args) throws Throwable {
        final long current = System.currentTimeMillis();

        System.out.println(" Trying to startup KlarCloudClient...");
        System.out.println(" Startup time: " + DateProvider.formatByDefaultFormat(current));

        System.out.println(" Trying to delete old \"logs\" directory...");

        if (Files.exists(Paths.get("klarcloud/logs")))
            FileUtils.deleteFullDirectory(Paths.get("klarcloud/logs"));

        System.out.println(" Old \"logs\" directory has been deleted");

        System.out.println();
        System.out.println(" Detecting KlarCloud Version...");
        if (System.getProperty("klarcloud.version") != null)
            if (!System.getProperty("klarcloud.version").equalsIgnoreCase(StringUtil.KLARCLOUD_VERSION)) {
                System.out.println(" KlarCloudVersion is not supported, refreshing System property...");
                System.setProperty("klarcloud.version", StringUtil.KLARCLOUD_VERSION);
                System.out.println(" KlarCloudVersion has been set");
            } else
                System.out.println(" Version is supported, continue...");
        else {
            System.out.println(" KlarCloudVersion System property is not set, setting now...");
            System.setProperty("klarcloud.version", StringUtil.KLARCLOUD_VERSION);
            System.out.println(" KlarCloudVersion has been set");
        }

        System.out.println(" Falling back to KlarCloudVersion " + StringUtil.KLARCLOUD_VERSION + "@" + StringUtil.KLARCLOUD_SPECIFICATION);
        System.out.println();

        KlarCloudLibrary.sendHeader();

        final OptionSet optionSet = new OptionParser().parse(args);

        final KlarCloudConsoleLogger klarCloudConsoleLogger = new KlarCloudConsoleLogger(optionSet.has("colour"));
        final CommandManager commandManager = new CommandManager();

        klarCloudConsoleLogger.info("Disabling ResourceLeakDetector...");

        ResourceLeakDetector.setLevel(ResourceLeakDetector.Level.DISABLED);

        klarCloudConsoleLogger.info("ResourceLeakDetector has been disabled");
        klarCloudConsoleLogger.info("Setting System-Properties...");

        System.setProperty("io.netty.maxDirectMemory", "0");
        System.setProperty("io.netty.leakDetectionLevel", "DISABLED");
        System.setProperty("file.encoding", "UTF-8");
        System.setProperty("java.net.preferIPv4Stack", "true");
        System.setProperty("io.netty.recycler.maxCapacity", "0");
        System.setProperty("io.netty.recycler.maxCapacity.default", "0");
        System.setProperty("io.netty.noPreferDirect", "true");
        System.setProperty("client.encoding.override", "UTF-8");

        klarCloudConsoleLogger.info("System-Properties are now ready");

        new KlarCloudClient(klarCloudConsoleLogger, commandManager, optionSet.has("ssl"), current);

        klarCloudConsoleLogger.info("Use the command \"help\" for help.");

        String line;
        try {
            while (true) {
                klarCloudConsoleLogger.getConsoleReader().setPrompt("");
                klarCloudConsoleLogger.getConsoleReader().resetPromptLine("", "", 0);

                while ((line = klarCloudConsoleLogger.getConsoleReader().readLine(" " + StringUtil.KLARCLOUD_VERSION + "-" + StringUtil.KLARCLOUD_SPECIFICATION + "@KlarCloudClient > ")) != null && KlarCloudClient.RUNNING) {
                    klarCloudConsoleLogger.getConsoleReader().setPrompt("");

                    if (!commandManager.dispatchCommand(line))
                        klarCloudConsoleLogger.info("Command not found! Use the command \"help\" for help.");
                }
            }
        } catch (final Throwable throwable) {
            throwable.printStackTrace();
        }
    }
}
