/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.meta.info;

import de.klarcloudservice.meta.CloudProcess;
import de.klarcloudservice.meta.server.ServerGroup;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

/**
 * @author _Klaro | Pasqual K. / created on 29.10.2018
 */

@AllArgsConstructor
@Getter
@Setter
public class ServerInfo implements Serializable {
    private static final long serialVersionUID = 8057730391607929124L;

    private CloudProcess cloudProcess;

    private ServerGroup serverGroup;

    private String group, host, motd;
    private int port, online, maxMemory;

    @Deprecated
    private boolean maintenance;
    private boolean ingame;

    private List<UUID> onlinePlayers;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ServerInfo)) return false;
        ServerInfo that = (ServerInfo) o;
        return getPort() == that.getPort() &&
                getOnline() == that.getOnline() &&
                getMaxMemory() == that.getMaxMemory() &&
                isMaintenance() == that.isMaintenance() &&
                isIngame() == that.isIngame() &&
                getCloudProcess().equals(that.getCloudProcess()) &&
                getServerGroup().equals(that.getServerGroup()) &&
                getGroup().equals(that.getGroup()) &&
                getHost().equals(that.getHost()) &&
                getMotd().equals(that.getMotd()) &&
                getOnlinePlayers().equals(that.getOnlinePlayers());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getCloudProcess(), getServerGroup(), getGroup(), getHost(), getMotd(), getPort(), getOnline(), getMaxMemory(), isMaintenance(), isIngame(), getOnlinePlayers());
    }

    @Override
    public String toString() {
        return "ServerInfo{" +
                "cloudProcess=" + cloudProcess +
                ", serverGroup=" + serverGroup +
                ", group='" + group + '\'' +
                ", host='" + host + '\'' +
                ", motd='" + motd + '\'' +
                ", port=" + port +
                ", online=" + online +
                ", maxMemory=" + maxMemory +
                ", maintenance=" + maintenance +
                ", ingame=" + ingame +
                ", onlinePlayers=" + onlinePlayers +
                '}';
    }
}
