/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.meta.permissions;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.Collection;

/**
 * @author _Klaro | Pasqual K. / created on 25.11.2018
 */

@AllArgsConstructor
@Getter
@Setter

@Deprecated
public class PermissionConfiguration {
    private boolean enabled;
    private Collection<PermissionGroup> permissionGroups;
}
