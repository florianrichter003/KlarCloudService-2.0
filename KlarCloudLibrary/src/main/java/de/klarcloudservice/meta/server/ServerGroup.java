/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.meta.server;

import de.klarcloudservice.meta.Template;
import de.klarcloudservice.meta.enums.ServerModeType;
import de.klarcloudservice.meta.server.configuration.AdvancedConfiguration;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Objects;

/**
 * @author _Klaro | Pasqual K. / created on 21.10.2018
 */

@AllArgsConstructor
@Getter
@Setter
public class ServerGroup implements Serializable {
    private static final long serialVersionUID = -6849497313084944255L;

    protected String name, client, motd, join_permission;
    protected int memory, minOnline, maxOnline, startPort, percentOfOnline;

    protected boolean maintenance;
    protected ServerModeType serverModeType;

    protected AdvancedConfiguration advancedConfiguration;

    protected Template template;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ServerGroup)) return false;
        ServerGroup that = (ServerGroup) o;
        return getMemory() == that.getMemory() &&
                getMinOnline() == that.getMinOnline() &&
                getMaxOnline() == that.getMaxOnline() &&
                getStartPort() == that.getStartPort() &&
                getPercentOfOnline() == that.getPercentOfOnline() &&
                isMaintenance() == that.isMaintenance() &&
                getName().equals(that.getName()) &&
                getClient().equals(that.getClient()) &&
                getMotd().equals(that.getMotd()) &&
                getJoin_permission().equals(that.getJoin_permission()) &&
                this.getServerModeType() == that.getServerModeType() &&
                getAdvancedConfiguration().equals(that.getAdvancedConfiguration()) &&
                getTemplate().equals(that.getTemplate());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getClient(), getMotd(), getJoin_permission(), getMemory(), getMinOnline(), getMaxOnline(), getStartPort(), getPercentOfOnline(), isMaintenance(), this.getServerModeType(), getAdvancedConfiguration(), getTemplate());
    }

    @Override
    public String toString() {
        return "ServerGroup{" +
                "name='" + name + '\'' +
                ", client='" + client + '\'' +
                ", motd='" + motd + '\'' +
                ", join_permission='" + join_permission + '\'' +
                ", memory=" + memory +
                ", minOnline=" + minOnline +
                ", maxOnline=" + maxOnline +
                ", startPort=" + startPort +
                ", percentOfOnline=" + percentOfOnline +
                ", maintenance=" + maintenance +
                ", serverModeType=" + serverModeType +
                ", advancedConfiguration=" + advancedConfiguration +
                ", template=" + template +
                '}';
    }
}
