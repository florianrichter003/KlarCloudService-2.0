/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.meta.server.defaults;

import de.klarcloudservice.meta.Template;
import de.klarcloudservice.meta.enums.ServerModeType;
import de.klarcloudservice.meta.enums.TemplateBackend;
import de.klarcloudservice.meta.server.ServerGroup;
import de.klarcloudservice.meta.server.configuration.AdvancedConfiguration;
import lombok.ToString;

import java.io.Serializable;

/**
 * @author _Klaro | Pasqual K. / created on 21.10.2018
 */

@ToString
public class LobbyGroup extends ServerGroup implements Serializable {

    private static final long serialVersionUID = -6740582229649845556L;

    public LobbyGroup() {
        super("Lobby", "Client-01", "KlarCloud | CloudServer", null, 512, 1, -1,
                41000, 80, false, ServerModeType.LOBBY,
                new AdvancedConfiguration(
                        false, false, true, true, true, false, true, false,
                        false, true, true, "world", "", 50, 10, 0, 1,
                        0, 265
                ), new Template("client", null, TemplateBackend.CLIENT)
        );
    }

}
