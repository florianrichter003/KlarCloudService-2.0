/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.meta.server.defaults;

import de.klarcloudservice.meta.Template;
import de.klarcloudservice.meta.enums.ServerModeType;
import de.klarcloudservice.meta.enums.TemplateBackend;
import de.klarcloudservice.meta.server.ServerGroup;
import de.klarcloudservice.meta.server.configuration.AdvancedConfiguration;

import java.io.Serializable;

/**
 * @author _Klaro | Pasqual K. / created on 09.12.2018
 */

public class DefaultGroup extends ServerGroup implements Serializable {
    private static final long serialVersionUID = -1234409573533112793L;

    public DefaultGroup(final String name, final String client) {
        super(name, client, "KlarCloud | CloudServer", null, 512, 1, -1,
                41000, 80, false, ServerModeType.GAME_SERVER,
                new AdvancedConfiguration(
                        false, false, true, true, true, false, true, false,
                        false, true, true, "world", "", 50, 10, 0, 1,
                        0, 265
                ), new Template("client", null, TemplateBackend.CLIENT)
        );
    }

    @Override
    public String toString() {
        return "DefaultGroup{}";
    }
}
