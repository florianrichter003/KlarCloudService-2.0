/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.meta.web;

import de.klarcloudservice.KlarCloudLibrary;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Map;
import java.util.Objects;

/**
 * @author _Klaro | Pasqual K. / created on 16.12.2018
 */

@AllArgsConstructor
@Getter
@Setter
public class WebUser implements Serializable {
    private static final long serialVersionUID = 6104918827767931388L;

    private String user, password;
    private Map<String, Boolean> permissions = KlarCloudLibrary.concurrentHashMap();

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof WebUser)) return false;
        WebUser webUser = (WebUser) o;
        return getUser().equals(webUser.getUser()) &&
                Objects.equals(getPassword(), webUser.getPassword());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getUser(), getPassword());
    }

    @Override
    public String toString() {
        return "WebUser{" +
                "user='" + user + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
