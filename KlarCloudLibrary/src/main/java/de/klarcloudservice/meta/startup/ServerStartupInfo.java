/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.meta.startup;

import de.klarcloudservice.configurations.Configuration;
import de.klarcloudservice.meta.server.ServerGroup;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

/**
 * @author _Klaro | Pasqual K. / created on 29.10.2018
 */

@AllArgsConstructor
@Getter
public class ServerStartupInfo implements Serializable {
    private static final long serialVersionUID = 3276684735275715610L;

    private UUID uid;
    private String name;
    private ServerGroup serverGroup;
    private Configuration configuration;
    private int id;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ServerStartupInfo)) return false;
        ServerStartupInfo that = (ServerStartupInfo) o;
        return getUid().equals(that.getUid()) &&
                getName().equals(that.getName()) &&
                getServerGroup().equals(that.getServerGroup()) &&
                getConfiguration().equals(that.getConfiguration());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getUid(), getName(), getServerGroup(), getConfiguration());
    }

    @Override
    public String toString() {
        return "ServerStartupInfo{" +
                "uid=" + uid +
                ", name='" + name + '\'' +
                ", serverGroup=" + serverGroup +
                ", configuration=" + configuration +
                '}';
    }
}
