package de.klarcloudservice.meta.proxy.configuration;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
@AllArgsConstructor
public class ProxyMaintenanceConfig implements Serializable {

    private Motd maintenanceMode;
    private String maintenanceProtocol;
    private List<String> maintenancePlayerInfo;

}
