/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.meta.proxy;

import de.klarcloudservice.meta.Template;
import de.klarcloudservice.meta.proxy.configuration.ProxyConfig;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Objects;

/**
 * @author _Klaro | Pasqual K. / created on 21.10.2018
 */

@AllArgsConstructor
@Data
public class ProxyGroup implements Serializable {

    private static final long serialVersionUID = 4196459006374952552L;

    protected String name, client;
    protected int startPort, minOnline, maxOnline, memory;

    protected ProxyConfig proxyConfig;
    protected Template template;
}
