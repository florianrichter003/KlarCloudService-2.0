/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.meta.proxy.configuration;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.UUID;

/**
 * @author _Klaro | Pasqual K. / created on 21.10.2018
 */

@AllArgsConstructor
@Data
public class ProxyConfig implements Serializable {

    private static final long serialVersionUID = 8806439752733227333L;

    private boolean maintenance, viaVersion, controllerCommandLogging, customProtocol;
    private Motd normalLayout;
    private int maxPlayers;
    private List<String> playerInfo;
    private String ip, customProtocolText;
    private Collection<UUID> whitelist;
    private Tablist tablist;
    private ProxyMaintenanceConfig proxyMaintenanceConfig;
}
