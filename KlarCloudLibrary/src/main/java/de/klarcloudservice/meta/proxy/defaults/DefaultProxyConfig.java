/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.meta.proxy.defaults;

import de.klarcloudservice.meta.proxy.configuration.Motd;
import de.klarcloudservice.meta.proxy.configuration.ProxyConfig;
import de.klarcloudservice.meta.proxy.configuration.ProxyMaintenanceConfig;
import de.klarcloudservice.meta.proxy.configuration.Tablist;
import de.klarcloudservice.utility.StringUtil;

import java.io.Serializable;
import java.util.Arrays;
import java.util.UUID;

/**
 * @author _Klaro | Pasqual K. / created on 21.10.2018
 */

final class DefaultProxyConfig extends ProxyConfig implements Serializable {

    private static final long serialVersionUID = 5559609589811160975L;

    DefaultProxyConfig(String host) {
        super(true, false, false, false,
                new Motd(true, "   §c§lK§4§llar§c§lC§4§lloud§c§lS§4§lervice§8│ §7An §4intelligent §7CloudSystem",
                        "    §8➥ §7Version §8» §4%version%§8│ §7Proxy §8» §4%proxy%"),
                512, Arrays.asList(
                        "§8§m-------------------------",
                        "§r          §c§lK§4§llar§c§lC§4§lloud§c§lS§4§lervice§8",
                        StringUtil.SPACE,
                        "§r   §7Twitter §8» §c@KlarCloud",
                        "§r   §7Teamspeak §8» §cts.klarcloudservice.de",
                        "§r   §7Discord §8» §cdiscord.klarcloudservice.de",
                        "§8§m-------------------------"
                ), host, "§c%online%§8/§c%max%", Arrays.asList(
                        UUID.fromString("776a08c7-01d7-4637-89b7-0026a04331ab"),
                        UUID.fromString("49304956-8cb9-4329-aaad-72c007b2003e"),
                        UUID.fromString("9a134431-c322-44c1-abd3-70ae2f1d9497"),
                        UUID.fromString("bcc582ed-494d-4b93-86cb-b58564651a26")),
                new Tablist(true, 1,
                        "§c§lK§4§llar§c§lC§4§lloud§c§lS§4§lervice§8│ §7An §4intelligent §7CloudSystem",
                        "§8➥ §7Version §8» §4%version%§8│ §7Proxy §8» §4%proxy%")
                , new ProxyMaintenanceConfig(new Motd(true,
                        "   §c§lK§4§llar§c§lC§4§lloud§c§lS§4§lervice§8│ §7An §4intelligent §7CloudSystem",
                        "    §8➥ §7Version §8» §4%version%§8│ §cMaintenance Mode"),
                        "§8» §cMaintenance",
                        Arrays.asList(
                                "§8§m-------------------------",
                                "§r          §c§lK§4§llar§c§lC§4§lloud§c§lS§4§lervice§8",
                                StringUtil.SPACE,
                                "§r   §7Twitter §8» §c@KlarCloud",
                                "§r   §7Teamspeak §8» §cts.klarcloudservice.de",
                                "§r   §7Discord §8» §cdiscord.klarcloudservice.de",
                                "§8§m-------------------------"
                        )));
    }
}
