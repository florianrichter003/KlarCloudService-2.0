/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.configurations;

import com.google.common.annotations.Beta;
import com.google.gson.JsonObject;
import de.klarcloudservice.KlarCloudLibrary;
import de.klarcloudservice.utility.StringUtil;
import lombok.Getter;
import lombok.Setter;

import java.io.*;
import java.lang.reflect.Type;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;

/**
 * @author _Klaro | Pasqual K. / created on 18.10.2018
 */

/**
 * Class to create easier Configuration files
 */

@Getter
@Setter
@Beta
public final class Configuration {
    protected JsonObject jsonObject;

    public Configuration() {
        this.jsonObject = new JsonObject();
    }

    public Configuration(JsonObject jsonObject) {
        this.jsonObject = jsonObject;
    }

    public Configuration addStringProperty(String key, String value) {
        this.jsonObject.addProperty(key, value);
        return this;
    }

    public Configuration addIntegerProperty(String key, Integer value) {
        this.jsonObject.addProperty(key, value);
        return this;
    }

    public Configuration addBooleanProperty(String key, Boolean value) {
        this.jsonObject.addProperty(key, value);
        return this;
    }

    public Configuration addConfigurationProperty(String key, Configuration value) {
        this.jsonObject.add(key, value.jsonObject);
        return this;
    }

    public Configuration addProperty(String key, Object value) {
        this.jsonObject.add(key, KlarCloudLibrary.GSON.toJsonTree(value));
        return this;
    }

    public Configuration remove(String key) {
        this.jsonObject.remove(key);
        return this;
    }

    public String getStringValue(String key) {
        return jsonObject.has(key) ? jsonObject.get(key).getAsString() : StringUtil.NULL;
    }

    public int getIntegerValue(String key) {
        return jsonObject.has(key) ? jsonObject.get(key).getAsInt() : 0;
    }

    public boolean getBooleanValue(String key) {
        return jsonObject.has(key) && jsonObject.get(key).getAsBoolean();
    }

    public <T> T getValue(String key, Class<T> clazz) {
        return jsonObject.has(key) ? KlarCloudLibrary.GSON.fromJson(jsonObject.get(key), clazz) : null;
    }

    public <T> T getValue(String key, Type type) {
        return jsonObject.has(key) ? KlarCloudLibrary.GSON.fromJson(jsonObject.get(key), type) : null;
    }

    public Configuration getConfiguration(String key) {
        return jsonObject.has(key) ? new Configuration(jsonObject.get(key).getAsJsonObject()) : null;
    }

    private boolean saveAsConfigurationFile(File backend) {
        if (backend.exists())
            backend.delete();

        try (OutputStreamWriter writer = new OutputStreamWriter(new FileOutputStream(backend), StandardCharsets.UTF_8.name())) {
            KlarCloudLibrary.GSON.toJson(jsonObject, (writer));
            return true;
        } catch (final IOException ex) {
            ex.getStackTrace();
        }
        return false;
    }

    public boolean saveAsConfigurationFile(Path path) {
        return this.saveAsConfigurationFile(path.toFile());
    }

    public static Configuration loadConfiguration(File file) {
        try (InputStreamReader reader = new InputStreamReader(Files.newInputStream(file.toPath()), StandardCharsets.UTF_8.name()); BufferedReader bufferedReader = new BufferedReader(reader)) {
            return new Configuration(KlarCloudLibrary.PARSER.parse(bufferedReader).getAsJsonObject());
        } catch (final IOException ex) {
            ex.getStackTrace();
        }
        return new Configuration();
    }

    public static Configuration loadConfiguration(Path path) {
        return loadConfiguration(path.toFile());
    }

    @Deprecated
    public Configuration clear() {
        this.jsonObject.entrySet().forEach(jsonObject -> remove(jsonObject.getKey()));
        return this;
    }

    public String getJsonString() {
        return this.jsonObject.toString();
    }

    public boolean contains(String key) {
        return this.jsonObject.has(key);
    }
}
