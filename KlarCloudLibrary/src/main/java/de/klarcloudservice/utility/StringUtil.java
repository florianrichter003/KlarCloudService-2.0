/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.utility;

import de.klarcloudservice.logging.KlarCloudConsoleLogger;

/**
 * @author _Klaro | Pasqual K. / created on 31.10.2018
 */

public final class StringUtil {
    public static final String JAVA = "java",
            JAVA_JAR = "-jar",
            EMPTY = "",
            SPACE = " ",
            SLASH = "/",
            BACK_SLASH = "\\",
            NULL = "null",
            OS_NAME = System.getProperty("os.name"),
            OS_ARCH = System.getProperty("os.arch"),
            OS_VERSION = System.getProperty("os.version"),
            USER_NAME = System.getProperty("user.name"),
            JAVA_VERSION = System.getProperty("java.version"),
            KLARCLOUD_SPECIFICATION = StringUtil.class.getPackage().getSpecificationVersion(),
            KLARCLOUD_VERSION = StringUtil.class.getPackage().getImplementationVersion(),
            LOGGER_INFO = "[§a%date%§r] ",
            LOGGER_WARN = "[§e%date%§r] ",
            LOGGER_ERR = "[§c%date%§r] ";

    private static final String[] unusedErrorComment = new String[] {
            "Who set up this error?", "Oh that was the idea", "This should happen?", "Yes", "I feel sad now :(", "It was my fault :(", "I\'m sorry _Klaro",
            "I will bring you a poster", "Thank you :)", "Surprise!, Haha just a tiny joke", "What?", "I\'m crashaholic", "Oof", "He. Stop!", "Ouch that hurts",
            "No problem my friend", "That was my present for you", "I\'m gonna fix this error", "Ok bro, do that"
    };

    public static void printError(final KlarCloudConsoleLogger klarCloudConsoleLogger, final String whoIAm, final Throwable cause) {
        try {
            for (String s : StringUtil.unusedErrorComment)
                klarCloudConsoleLogger.err(s);
        } catch (final Throwable ignored) {
            klarCloudConsoleLogger.err("No comment :(");
        }

        klarCloudConsoleLogger.emptyLine().err("---------------------------------------------------");
        klarCloudConsoleLogger.emptyLine().err(whoIAm);
        klarCloudConsoleLogger.err("If you have an addon which changes something and you see it in the Exception dump below, please report it to the author");
        klarCloudConsoleLogger.err("If you are unsure or still think that this is a KlarCloud Bug, please visit and report to https://discord.gg/fwe2CHD");
        klarCloudConsoleLogger.err("Be sure to include ALL relevant console errors and Log files");
        klarCloudConsoleLogger.err("KlarCloud Version: " + KLARCLOUD_VERSION);
        klarCloudConsoleLogger.err("KlarCloud Specification: " + KLARCLOUD_SPECIFICATION);
        klarCloudConsoleLogger.emptyLine();
        cause.printStackTrace();
        klarCloudConsoleLogger.emptyLine().err("---------------------------------------------------");
    }
}
