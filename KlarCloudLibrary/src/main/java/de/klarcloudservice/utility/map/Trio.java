/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.utility.map;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author _Klaro | Pasqual K. / created on 30.10.2018
 */

@Getter
@AllArgsConstructor
public class Trio<F, S, T> {
    private F first;
    private S second;
    private T third;
}
