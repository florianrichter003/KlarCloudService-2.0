package de.klarcloudservice.utility.uuid;

import java.util.UUID;
import java.util.regex.Pattern;

public class UUIDConverter {

    /**
     * From https://stackoverflow.com/a/47238049/10939910
     */
    private static final Pattern PATTERN = Pattern.compile("(\\w{8})(\\w{4})(\\w{4})(\\w{4})(\\w{12})");

    /**
     * convert a uuid string without dashes to a uuid object
     *
     * @param rawUuid the raw uuid without dashes
     * @return the final uuid
     */
    public static UUID toUUID(String rawUuid) {
        if (rawUuid.length() != 32)
            throw new IllegalArgumentException("The rawuuid must be 32 char long.");
        return UUID.fromString(UUIDConverter.PATTERN.matcher(rawUuid).replaceAll("$1-$2-$3-$4-$5"));
    }
}
