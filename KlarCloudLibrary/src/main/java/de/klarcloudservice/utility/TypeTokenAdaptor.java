/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.utility;

import com.google.gson.reflect.TypeToken;
import de.klarcloudservice.meta.info.ProxyInfo;
import de.klarcloudservice.meta.info.ServerInfo;
import de.klarcloudservice.meta.proxy.ProxyGroup;
import de.klarcloudservice.meta.server.ServerGroup;
import de.klarcloudservice.meta.startup.ProxyStartupInfo;
import de.klarcloudservice.meta.startup.ServerStartupInfo;
import de.klarcloudservice.netty.authentication.enums.AuthenticationType;
import de.klarcloudservice.netty.packet.Packet;
import de.klarcloudservice.signs.SignLayoutConfiguration;
import de.klarcloudservice.utility.cloudsystem.InternalCloudNetwork;
import de.klarcloudservice.utility.cloudsystem.KlarCloudAddresses;
import lombok.Getter;

import java.lang.reflect.Type;

/**
 * @author _Klaro | Pasqual K. / created on 05.12.2018
 */

public final class TypeTokenAdaptor {
    @Getter
    private static final Type
            serverGroupType = new TypeToken<ServerGroup>() {
    }.getType(),
            proxyGroupType = new TypeToken<ProxyGroup>() {
            }.getType(),
            serverInfoType = new TypeToken<ServerInfo>() {
            }.getType(),
            proxyInfoType = new TypeToken<ProxyInfo>() {
            }.getType(),
            serverStartupInfoType = new TypeToken<ServerStartupInfo>() {
            }.getType(),
            proxyStartupInfoType = new TypeToken<ProxyStartupInfo>() {
            }.getType(),
            internalCloudNetworkType = new TypeToken<InternalCloudNetwork>() {
            }.getType(),
            packetType = new TypeToken<Packet>() {
            }.getType(),
            klarCloudAddressesType = new TypeToken<KlarCloudAddresses>() {
            }.getType(),
            authenticationType = new TypeToken<AuthenticationType>() {
            }.getType(),
            signLayoutConfigType = new TypeToken<SignLayoutConfiguration>() {
            }.getType();
}
