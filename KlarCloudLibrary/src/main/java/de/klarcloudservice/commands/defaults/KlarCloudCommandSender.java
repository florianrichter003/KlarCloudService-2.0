/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.commands.defaults;

import de.klarcloudservice.KlarCloudLibraryService;
import de.klarcloudservice.commands.interfaces.CommandSender;

/**
 * @author _Klaro | Pasqual K. / created on 18.10.2018
 */

/**
 * Default CommandSender instance for all Console Commands
 */
public class KlarCloudCommandSender implements CommandSender {
    @Override
    public void sendMessage(String message) {
        KlarCloudLibraryService.getInstance().getKlarCloudConsoleLogger().info(message);
    }

    @Override
    public boolean hasPermission(String permission) {
        return true;
    }
}
