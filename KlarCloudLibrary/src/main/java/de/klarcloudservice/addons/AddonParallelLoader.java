/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.addons;

import de.klarcloudservice.KlarCloudLibraryService;
import de.klarcloudservice.addons.configuration.AddonClassConfig;
import de.klarcloudservice.addons.exceptions.AddonLoadException;
import de.klarcloudservice.addons.extendable.AddonExtendable;
import de.klarcloudservice.addons.loader.AddonMainClassLoader;
import de.klarcloudservice.utility.StringUtil;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

/**
 * @author _Klaro | Pasqual K. / created on 10.12.2018
 */

public class AddonParallelLoader extends AddonExtendable {
    private Queue<JavaAddon> javaAddons = new ConcurrentLinkedDeque<>();

    /**
     * Loads all addons
     */
    @Override
    public void loadAddons() {
        Collection<AddonClassConfig> addonClassConfigs = this.checkForAddons();

        addonClassConfigs.forEach(addonClassConfig -> {
            KlarCloudLibraryService.getInstance().getKlarCloudConsoleLogger().info("Preparing Addon [Name=" + addonClassConfig.getName() + "/Version=" + addonClassConfig.getVersion() + "]...");

            try {
                final AddonMainClassLoader addonMainClassLoader = new AddonMainClassLoader(addonClassConfig);
                JavaAddon javaAddon = addonMainClassLoader.loadAddon();
                javaAddon.setAddonMainClassLoader(addonMainClassLoader);

                javaAddons.add(javaAddon);

                KlarCloudLibraryService.getInstance().getKlarCloudConsoleLogger().info("Addon [Name=" + addonClassConfig.getName() + "/Version=" + addonClassConfig.getVersion() + "] has been prepared");
            } catch (final Throwable throwable) {
                StringUtil.printError(KlarCloudLibraryService.getInstance().getKlarCloudConsoleLogger(), "Error while loading addon", throwable);
            }
        });
    }

    /**
     * Enables the Addons
     */
    @Override
    public void enableAddons() {
        this.javaAddons.forEach(consumer -> {
            KlarCloudLibraryService.getInstance().getKlarCloudConsoleLogger().info("Enabling Addon [Name=" + consumer.getModuleName() + "/Version=" + consumer.getAddonClassConfig().getVersion() + "]...");
            consumer.onModuleLoading();
            KlarCloudLibraryService.getInstance().getKlarCloudConsoleLogger().info("Addon [Name=" + consumer.getModuleName() + "/Version=" + consumer.getAddonClassConfig().getVersion() + "] has been enabled");
        });
    }

    /**
     * Disables all addons
     */
    @Override
    public void disableAddons() {
        if (javaAddons.isEmpty())
            return;

        do {
            JavaAddon consumer = javaAddons.poll();
            KlarCloudLibraryService.getInstance().getKlarCloudConsoleLogger().info("Closing Addon [Name=" + consumer.getModuleName() + "/Version=" + consumer.getAddonClassConfig().getVersion() + "]...");
            consumer.onModuleReadyToClose();
            KlarCloudLibraryService.getInstance().getKlarCloudConsoleLogger().info("Addon [Name=" + consumer.getModuleName() + "/Version=" + consumer.getAddonClassConfig().getVersion() + "] has been closed");

        } while (!javaAddons.isEmpty());
    }

    /**
     * Returns all Addon Class Configs of all jar files
     *
     * @return {@link Set<AddonClassConfig>} of all Addons
     */
    private Set<AddonClassConfig> checkForAddons() {
        Set<AddonClassConfig> moduleConfigs = new HashSet<>();

        File[] files = new File("klarcloud/addons").listFiles(pathname ->
                pathname.isFile()
                        && pathname.exists()
                        && pathname.getName().endsWith(".jar"));
        if (files == null)
            return moduleConfigs;

        for (File file : files) {
            try (JarFile jarFile = new JarFile(file)) {
                JarEntry jarEntry = jarFile.getJarEntry("addon.properties");
                if (jarEntry == null)
                    throw new AddonLoadException(new FileNotFoundException("Could't find properties file"));

                try (InputStreamReader reader = new InputStreamReader(jarFile.getInputStream(jarEntry), StandardCharsets.UTF_8)) {
                    Properties properties = new Properties();
                    properties.load(reader);
                    AddonClassConfig moduleConfig = new AddonClassConfig(file,
                            properties.getProperty("name"),
                            properties.getProperty("version"),
                            properties.getProperty("main"));
                    moduleConfigs.add(moduleConfig);
                }
            } catch (final Throwable throwable) {
                throwable.printStackTrace();
            }
        }

        return moduleConfigs;
    }
}
