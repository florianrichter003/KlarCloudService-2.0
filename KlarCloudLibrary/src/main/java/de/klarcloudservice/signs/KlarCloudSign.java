/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.signs;

import de.klarcloudservice.meta.info.ServerInfo;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

/**
 * @author _Klaro | Pasqual K. / created on 11.12.2018
 */

@AllArgsConstructor
@Getter
public class KlarCloudSign {
    private UUID uuid;
    private SignPosition signPosition;

    @Setter
    private ServerInfo serverInfo;
}
